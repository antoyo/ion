{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Lexer
Description: The ion lexer.
-}

module Lexer (ParserPos, braces, charLiteral, colon, commaSep, commaSep1, customOperatorIdDef, decimal, hole, identifier, majIdentifier, operator, parens, reserved, reservedOp, stringLiteral, naturalOrFloat, whiteSpace, wrapInPos, wrapInPosUntyped) where

import Control.Monad.State (State)

import Text.Parsec ((<|>), alphaNum, char, getPosition, lower, many, oneOf, upper)
import Text.Parsec.Indent (IndentParser)
import Text.Parsec.Language (emptyDef)
import Text.Parsec.Pos (SourcePos)
import Text.Parsec.Token (GenTokenParser, caseSensitive, commentEnd, commentLine, commentStart, identLetter, identStart, makeTokenParser, nestedComments, opLetter, opStart, reservedNames, reservedOpNames)
import qualified Text.Parsec.Token as Token

import Ast (WithType, untyped)
import Position (WithPos(..))

type ParserIndent a = IndentParser String () a
type ParserPos a = ParserIndent (WithPos a)
type TokenParser st = GenTokenParser String st (State SourcePos)

-- |The Ion lexer.
lexer :: TokenParser ()
lexer = makeTokenParser $ emptyDef
    { commentStart = "/*"
    , commentEnd = "*/"
    , commentLine = "//"
    , nestedComments = True
    , identStart = lower
    , identLetter = alphaNum <|> char '_'
    , opStart = operatorChar
    , opLetter = operatorChar
    , reservedNames = keywords
    , reservedOpNames = operators
    , caseSensitive = True
    }

braces :: ParserIndent a -> ParserIndent a
braces = Token.braces lexer

-- |The lexical units.

charLiteral :: ParserIndent Char
charLiteral = Token.charLiteral lexer

colon :: ParserIndent String
colon = Token.colon lexer

commaSep :: ParserIndent a -> ParserIndent [a]
commaSep = Token.commaSep lexer

commaSep1 :: ParserIndent a -> ParserIndent [a]
commaSep1 = Token.commaSep1 lexer

customOperatorIdDef :: ParserPos String
customOperatorIdDef = do
    _ <- char '`'
    operatorId <- operator
    _ <- char '`'
    whiteSpace
    return operatorId

decimal :: ParserIndent Integer
decimal =
    Token.decimal lexer <*
    whiteSpace

-- |An hole is char '_'.
hole :: ParserIndent ()
hole = do
    _ <- char '_'
    whiteSpace

-- |An identifier starts with an lowercase letter.
identifier :: ParserPos String
identifier = wrapInPos $ Token.identifier lexer

-- |An identifier starting with an uppercase letter.
majIdentifier :: ParserPos String
majIdentifier = wrapInPos $ do
    start <- upper
    rest <- many alphaNum
    whiteSpace
    return $ start : rest

operator :: ParserPos String
operator = wrapInPos $ Token.operator lexer

parens :: ParserIndent a -> ParserIndent a
parens = Token.parens lexer

reserved :: String -> ParserIndent ()
reserved = Token.reserved lexer

reservedOp :: String -> ParserIndent ()
reservedOp = Token.reservedOp lexer

stringLiteral :: ParserIndent String
stringLiteral = Token.stringLiteral lexer

naturalOrFloat :: ParserIndent (Either Integer Double)
naturalOrFloat = Token.naturalOrFloat lexer

whiteSpace :: ParserIndent ()
whiteSpace = Token.whiteSpace lexer

-- |Language keywords (reserved identifiers).
keywords :: [String]
keywords =
    [ "else"
    , "enum"
    , "if"
    , "let"
    , "mut"
    , "struct"
    , "then"
    , "type"
    ]

-- |Characters permitted in operators.
operatorChar :: ParserIndent Char
operatorChar = oneOf ":!#$%&*+./<=>?@\\^|-~"

-- |Language reserved operators.
operators :: [String]
operators = []

-- |Parse and wrap the result in its position.
wrapInPos :: ParserIndent a -> ParserIndent (WithPos a)
wrapInPos parser = do
    position <- getPosition
    item <- parser
    return WithPos { item, position }

-- |Parse and wrap the result in its position and untyped.
wrapInPosUntyped :: ParserIndent a -> ParserIndent (WithType (WithPos a))
wrapInPosUntyped parser = do
    position <- getPosition
    item <- parser
    return $ untyped WithPos { item, position }
