{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: AnalyzerState
Description: The semantic analyzer state.
-}

module AnalyzerState
    ( AnalyzerState
    , AnalyzerRecord(..)
    , EnumValueTable
    , Error
    , TypeTable
    , addEnumValue
    , addMethod
    , addType
    , getEnumValue
    , getEnv
    , getMethod
    , getType
    , modifyEnv
    , modifyEnvironment
    ) where

import Control.Monad.State (gets, modify)
import Control.Monad.Trans.State (StateT)
import qualified Data.Map as Map

import Text.Parsec.Error (ParseError)

import Ast ( EnumValue(..)
           , FunctionDefinition(..)
           , TypeDefinition(..)
           )
import Environment (Environment)
import GenState (CompilerFunctions, EnumValueTable, TypeTable)
import Position (WithPos(..))

-- |The semantic analyzer state record.
data AnalyzerRecord = AnalyzerRecord
                    { debugMode :: Bool
                    , enumValueTable :: EnumValueTable
                    , environment :: Environment
                    , compilerFuncs :: CompilerFunctions
                    , methodTable :: MethodTable
                    , patternIndex :: Int
                    , typeTable :: TypeTable
                    }

-- |The semantic analyzer state.
type AnalyzerState a = StateT AnalyzerRecord (Either Error) a

type Error = ParseError

-- |The method table.
type MethodTable = Map.Map String (Map.Map String FunctionDefinition)

-- |Add an enum value in the enum values table. Return the enum value name if it already exists.
addEnumValue :: String -> EnumValue -> AnalyzerState (Maybe String)
addEnumValue name enumV = do
    enums <- gets enumValueTable
    enum <- getEnumValue name
    case enum of
         Just _ -> return $ Just name
         Nothing -> do
             modify $ \state -> state { enumValueTable = Map.insert name enumV enums }
             return Nothing

-- |Add methods to the methods table. Return the method name if it already exists
addMethod :: String -> FunctionDefinition -> AnalyzerState (Maybe String)
addMethod structName structMethod@FunctionDefinition { functionName } = do
    methods <- gets methodTable
    let currentMethods = Map.findWithDefault Map.empty structName methods
    method <- getMethod structName (item functionName)
    case method of
         Just _ -> return $ Just (item functionName)
         Nothing -> do
             let newMethods = Map.insert (item functionName) structMethod currentMethods
             modify $ \state -> state { methodTable = Map.insert structName newMethods methods }
             return Nothing

-- |Add a type to the type table. Return the type name if it already exists.
addType :: String -> TypeDefinition -> AnalyzerState (Maybe String)
addType name typeDef = do
    types <- gets typeTable
    typ <- getType name
    case typ of
         Just _ -> return $ Just name
         Nothing -> do
             modify $ \state -> state { typeTable = Map.insert name typeDef types }
             return Nothing

-- |Get an enum value from the enum value table.
getEnumValue :: String -> AnalyzerState (Maybe EnumValue)
getEnumValue name = do
    enums <- gets enumValueTable
    return $ Map.lookup name enums

-- |Get the environment
getEnv :: AnalyzerState Environment
getEnv = gets environment

-- |Get a method from the method table.
getMethod :: String -> String -> AnalyzerState (Maybe FunctionDefinition)
getMethod structName methodName = do
    methods <- gets methodTable
    return $
        case Map.lookup structName methods of
             Just struct -> Map.lookup methodName struct
             Nothing -> Nothing

-- |Get a type from the type table.
getType :: String -> AnalyzerState (Maybe TypeDefinition)
getType name = do
    types <- gets typeTable
    return $ Map.lookup name types

-- |Function to modify the environment.
modifyEnv :: (Environment -> Environment) -> AnalyzerState ()
modifyEnv modificator = do
    environment <- gets environment
    let newEnv = modificator environment
    modify $ \state -> state { environment = newEnv }

-- |Function to modify the environment. Return a String from Environment module in case of error.
modifyEnvironment :: (Environment -> Either String Environment) -> AnalyzerState (Maybe String)
modifyEnvironment modificator = do
    environment <- gets environment
    case modificator environment of
        Left err -> return $ Just err
        Right env -> do
            modify $ \state -> state { environment = env }
            return Nothing
