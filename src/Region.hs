{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Region
Description: The region inference.
-}

module Region (regionInference) where

import Control.Monad.State (State, evalState, gets, modify)
import qualified Data.Map as Map

import Ast
    ( BinaryOperation(..)
    , Constant(..)
    , Definition(..)
    , ExprEscape(..)
    , Expression
    , Expression'(..)
    , FunctionCall(..)
    , FunctionDefinition(..)
    , IfExpression(..)
    , InlineAssembly(..)
    , LetStatement(..)
    , Module
    , Pattern(..)
    , Statement(..)
    , StatementPos
    , Ty(..)
    , Type(..)
    , UnaryOperation(..)
    , WithType(..)
    )
import Escape (Functions)
import Position (WithPos(..), dummyPos)

-- |The region inference record.
data RegionRecord =
    RegionRecord
    { currentFunction :: String
    , functions :: Functions
    }

-- |The region inference state.
type RegionState = State RegionRecord

-- |Infer the regions in a definition.
-- |If a function call is assigned to an escaping variable, send the region the function receive as a parameter.
-- |If the variable does not escape, send the region of the current function.
-- |If the function has escaping variables, add a parameter for the region.
inferDef :: Definition -> RegionState Definition
inferDef (Function function@FunctionDefinition { functionHasEscapes, functionName, functionParameters, functionStatements = WithType { itemNode = statements, itemType }, functionType = functionType@WithPos { item = FunctionType types }}) = do
    setCurrentFunction $ item functionName
    let (newParams, newType) =
            if functionHasEscapes then
                let regionParam = dummyPos $ Ident $ dummyPos "region$0"
                    regionType = dummyPos $ Type $ dummyPos "SubRegion"
                    funcType = functionType { item = FunctionType (regionType : types) }
                in
                (regionParam : functionParameters, funcType)
            else
                (functionParameters, functionType)
    newStatements <- mapM inferStatement statements
    let newFunctionStatements = WithType { itemType, itemNode = newStatements }
    return $ Function function { functionParameters = newParams, functionStatements = newFunctionStatements, functionType = newType }
inferDef (Function _) = fail "A function should have a function type."

inferDef (MethodDefinition _ _) = fail "A method should have been converted to a function."
inferDef typeDef@(TypeDef _ _) = return typeDef

-- |Infer the region to add to a function call expression.
inferExpr :: Expression -> RegionState Expression
inferExpr withType@WithType { itemNode = withPos@WithPos { item = expr }} = do
    newExpr <-
        case expr of
             BinOp (BinaryOperation op expr1 expr2) -> do
                 newExpr1 <- inferExpr expr1
                 newExpr2 <- inferExpr expr2
                 return $ BinOp $ BinaryOperation op newExpr1 newExpr2
             Call escape functionCall -> Call escape <$> inferFunctionCall escape functionCall
             char@(CharLiteral _) -> return char
             constant@(Const _) -> return constant
             double@(DoubleLiteral _) -> return double
             enum@(EnumExpression _) -> return enum
             field@(Field _) -> return field
             string@(GlobalString _) -> return string
             If IfExpression { condition, elseStatements, trueStatements } -> do
                 newCondition <- inferExpr condition
                 newElseStatements <- case elseStatements of
                      Just statements -> Just <$> mapM inferStatement statements
                      Nothing -> return Nothing
                 newTrueStatements <- mapM inferStatement trueStatements
                 return $ If IfExpression
                   { condition = newCondition
                   , elseStatements = newElseStatements
                   , trueStatements = newTrueStatements
                   }
             InlineAsm inlineAsm@InlineAssembly { inputArguments } -> do
                 newArgs <- mapM inferExpr inputArguments
                 return $ InlineAsm inlineAsm { inputArguments = newArgs }
             int@(IntegerLiteral _) -> return int
             MethodCall _ -> fail "A method call should have been converted to a function call."
             string@(StringLiteral _) -> return string
             StructExpression _ _ -> fail "A struct expression should have been converted to a tuple."
             Tuple structType escapeScope expressions -> do
                 newExprs <- mapM inferExpr expressions
                 return $ Tuple structType escapeScope newExprs
             UnOp (UnaryOperation op expression) -> do
                 newExpr <- inferExpr expression
                 return $ UnOp $ UnaryOperation op newExpr
    return withType { itemNode = withPos { item = newExpr }}

-- |Infer the region to add to a function call.
inferFunctionCall :: ExprEscape -> FunctionCall -> RegionState FunctionCall
inferFunctionCall escape functionCall@FunctionCall { callArguments, callName } = do
    functions <- gets functions
    newArgs <- mapM inferExpr callArguments
    let newFunc = functionCall { callArguments = newArgs }

    currentFunctionName <- gets currentFunction

    let newFuncCall =
            case Map.lookup (item callName) functions of
                 Just function ->
                    if functionHasEscapes function then
                        let region =
                                if escape == Escape then
                                    "region$0"
                                else
                                    "region$" ++ currentFunctionName
                        in
                        newFunc { callArguments = WithType
                                    { itemNode = dummyPos $ Const Constant { constantName = dummyPos region }
                                    , itemType = Ty "Region"
                                    }
                                : newArgs
                        }
                    else
                        newFunc
                 Nothing -> newFunc
    return newFuncCall

-- |Infer the region to add to a function call in a statement.
inferStatement :: StatementPos -> RegionState StatementPos
inferStatement statement@WithPos { item = Expr expression } = do
    newExpr <- inferExpr expression
    return statement { item = Expr newExpr }
inferStatement statement@WithPos { item = Let letStatement@LetStatement { letStatementValue }} = do
    newExpr <- inferExpr letStatementValue
    return statement { item = Let letStatement { letStatementValue = newExpr }}

-- |Do the region inference over a whole module.
-- |This will set a region to each call returning an escaping value.
regionInference :: (Module, Functions) -> Module
regionInference (modul, functions) =
    evalState (mapM inferDef modul) RegionRecord { currentFunction = "", functions }

-- |Set the current function name in the region state.
setCurrentFunction :: String -> RegionState ()
setCurrentFunction currentFunction =
    modify $ \s -> s { currentFunction }
