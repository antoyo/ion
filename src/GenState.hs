{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: GenState
Description: The code generator state.
-}

module GenState
    ( CompilerFunction(..)
    , CompilerFunctions
    , EnumValueTable
    , GenRecord(..)
    , GenState
    , TypeTable
    , addBlock
    , addGlobal
    , addInstr
    , addOperand
    , addUniqueBlock
    , allocMemory
    , callInstr
    , clearBlocks
    , genFieldStore
    , genGetRegion
    , getBlocks
    , getEnumValue
    , getOperand
    , getFieldPointer
    , getType
    , enterScope
    , leaveScope
    , nextName
    , nextUnName
    , regionPointerType
    , regionSize
    , regionType
    , setBlock
    , setFunction
    , setTerm
    , stringType
    , subRegionCurrentAddressField
    , subRegionNextField
    , subRegionSizeField
    , subRegionStartField
    , subRegionPointerType
    , subRegionType
    ) where

import Control.Monad.State (State, gets, modify)
import qualified Data.Map as Map

import LLVM.General.AST (BasicBlock(BasicBlock))
import LLVM.General.AST.AddrSpace (AddrSpace(AddrSpace))
import LLVM.General.AST.Constant (Constant(GlobalReference, Int))
import qualified LLVM.General.AST as AST
import LLVM.General.AST.CallingConvention (CallingConvention(C))
import qualified LLVM.General.AST.Instruction as Instruction
import LLVM.General.AST.Instruction (Instruction, Named((:=), Do), Terminator)
import LLVM.General.AST.Name (Name(Name, UnName))
import LLVM.General.AST.Operand (Operand(ConstantOperand, LocalReference))
import qualified LLVM.General.AST.Type as AST_Type
import LLVM.General.AST.Type (Type(NamedTypeReference, PointerType), i64, i8, void)

import Ast (EnumValue, ExprEscape(..), Ty, TypeDefinition)
import qualified Environment
import MaybeUtils (fromJustOrFail)

-- |A function defined in the compiler.
data CompilerFunction =
    CompilerFunction
    { compilerFunctionEscape :: Bool
    , compilerFunctionLLVM :: [Operand] -> ExprEscape -> AST_Type.Type -> GenState Operand
    , compilerFunctionType :: [Ty]
    }

-- |A collection of compiler functions.
type CompilerFunctions = Map.Map String CompilerFunction

-- |The generator state record.
data GenRecord = GenRecord
               { blocks :: Map.Map Name Block
               , compilerFuncs :: CompilerFunctions
               , currentBlock :: Name
               , currentFunction :: String
               , enumValueTable :: EnumValueTable
               , environment :: Environment.Environment
               , globals :: [AST.Definition]
               , stringIndex :: Int
               , typeTable :: TypeTable
               , unNameIndex :: Word
               }

-- |A basic block.
data Block = Block
           { instrs :: [Named Instruction]
           , terminator :: Maybe (Named Terminator)
           }

-- |The enum values table.
type EnumValueTable = Map.Map String EnumValue

-- |The generator state.
type GenState a = State GenRecord a

-- |The type table.
type TypeTable = Map.Map String TypeDefinition

-- |Add a new block.
addBlock :: String -> GenState Name
addBlock name = do
    modify update
    return llvmName
    where llvmName = Name name
          update oldState@GenRecord { blocks } =
            oldState { blocks = Map.insert llvmName newBlock blocks }
          newBlock = Block
                   { instrs = []
                   , terminator = Nothing
                   }

-- |Add a global definition (for instance, a const String) to the generator state.
addGlobal :: AST.Definition -> GenState ()
addGlobal definition =
    modify update
    where update oldState@GenRecord { globals } =
            oldState { globals = definition : globals }

-- |Add an instruction to the current basic block.
addInstr :: Named Instruction -> GenState ()
addInstr instruction =
    modifyBlock update
    where update block@Block { instrs } = block { instrs = instrs ++ [instruction] }

-- |Add an operand to the operand table.
addOperand :: String -> Operand -> GenState ()
addOperand name operand = do
    env <- gets environment
    let newEnv = Environment.addOperand name operand env
    modify $ \state -> state { environment = newEnv }

-- |Add a new block with an unique name.
addUniqueBlock :: String -> GenState Name
addUniqueBlock name = do
    newName <- nextName name
    modify $ update newName
    return newName
    where update llvmName oldState@GenRecord { blocks } =
            oldState { blocks = Map.insert llvmName newBlock blocks }
          newBlock = Block
                   { instrs = []
                   , terminator = Nothing
                   }

-- |Create the instructions to alloc heap memory.
allocMemory :: Operand -> String -> GenState Operand
allocMemory sizeOperand region = do
    let subRegion = LocalReference subRegionPointerType $ Name region
        regionAllocFunc = Right $ ConstantOperand $ GlobalReference void $ Name "SubRegion$alloc"
        regionAllocCall = callInstr regionAllocFunc [subRegion, sizeOperand]
    address <- nextUnName
    addInstr $ address := regionAllocCall
    return $ LocalReference i64 address

-- |Create a call instruction.
callInstr :: AST.CallableOperand -> [Operand] -> Instruction
callInstr func params =
    let paramsWithMetadata = map (\param -> (param, [])) params
    in
    Instruction.Call Nothing C [] func paramsWithMetadata [] []

-- |Clear the blocks from the State.
clearBlocks :: GenState ()
clearBlocks =
    modify $ \state -> state { blocks = Map.empty }

-- |Enter a new scope. Push a new scope in environment.
enterScope :: GenState ()
enterScope = do
    env <- gets environment
    let newEnv = Environment.enterScope env
    modify $ \state -> state { environment = newEnv }

-- |Generate the code to access a LLVM struct's field.
genFieldStore :: Operand -> AST_Type.Type -> Integer -> Operand -> GenState ()
genFieldStore struct llvmType index fieldNewValue = do
    field <- getFieldPointer struct llvmType index
    addInstr $ Do $ Instruction.Store False field fieldNewValue Nothing 0 []

-- |Generate the code to get the global region.
genGetRegion :: GenState Operand
genGetRegion = do
     let regionName = Name "programRegion"
         globalRegion = ConstantOperand $ GlobalReference void $ Name "globalProgramRegion"
     addInstr $ regionName := Instruction.GetElementPtr True globalRegion [ConstantOperand $ Int 64 0] []
     return $ LocalReference regionPointerType regionName

-- |Get the current basic blocks.
getBlocks :: GenState [BasicBlock]
getBlocks = do
    currentBlocks <- gets blocks
    return $ Map.elems $ Map.mapWithKey (\name@(Name termName) block ->
            let term = fromJustOrFail ("getBlocks: Block `" ++ termName ++ "` has no terminator.") $ terminator block
            in
            BasicBlock name (instrs block) term
        ) currentBlocks

-- |Get an enum value from the enum value table.
getEnumValue :: String -> GenState EnumValue
getEnumValue name = do
    enums <- gets enumValueTable
    return $ fromJustOrFail ("getEnumValue: Enum value `" ++ name ++ "` does not exist.") $
        Map.lookup name enums

-- |Compute a structure field pointer.
getFieldPointer :: Operand -> AST_Type.Type -> Integer -> GenState Operand
getFieldPointer struct llvmType index = do
    fieldAddr <- nextUnName
    let structIndex = ConstantOperand $ Int 64 0
        fieldIndex = ConstantOperand $ Int 32 index
        llvmAddrType = PointerType llvmType $ AddrSpace 0
    addInstr $ fieldAddr := Instruction.GetElementPtr True struct [structIndex, fieldIndex] []
    return $ LocalReference llvmAddrType fieldAddr

-- |Get an operand from the operand table.
getOperand :: String -> GenState Operand
getOperand name = do
    env <- gets environment
    funcName <- gets currentFunction
    return $ fromJustOrFail ("getOperand: Operand `" ++ name ++ "` does not exist in function " ++ funcName ++ ".") $
        Environment.getOperand env name

-- |Get a type from the type table.
getType :: String -> GenState (Maybe TypeDefinition)
getType name = do
    types <- gets typeTable
    return $ Map.lookup name types

-- |Leave a scope. Pop a scope in environment.
leaveScope :: GenState ()
leaveScope = do
    env <- gets environment
    let newEnv = Environment.leaveScope env
    modify $ \state -> state { environment = newEnv }

-- |Modify the current block.
modifyBlock :: (Block -> Block) -> GenState ()
modifyBlock updateBlock =
    modify update
    where update oldState@GenRecord { blocks, currentBlock } =
            case currentBlock of
                Name blockName ->
                    oldState { blocks = Map.insert currentBlock newBlock blocks }
                        where block = fromJustOrFail ("modifyBlock: Block `" ++ blockName ++ "` does not exist.") $ Map.lookup currentBlock blocks
                              newBlock = updateBlock block
                UnName _ -> error "modifyBlock: Block UnName"

-- |Return a name starting with `start` with the current `stringIndex` and go to the next.
nextName :: String -> GenState Name
nextName start = do
    index <- gets stringIndex
    modify update
    return $ Name $ start ++ show index
    where update oldState@GenRecord { stringIndex } =
            oldState { stringIndex = stringIndex + 1 }

-- |Return the current unName index and go to the next.
nextUnName :: GenState Name
nextUnName = do
    unName <- gets unNameIndex
    modify update
    return $ UnName unName
    where update oldState@GenRecord { unNameIndex } =
            oldState { unNameIndex = unNameIndex + 1 }

-- |The region structure type.
regionType :: AST_Type.Type
regionType = NamedTypeReference $ Name "Region"

regionPointerType :: AST_Type.Type
regionPointerType = PointerType regionType $ AddrSpace 0

-- |Initial region size.
regionSize :: Integer
regionSize = 5000

-- |Set the current block.
setBlock :: Name -> GenState ()
setBlock currentBlock =
    modify $ \s -> s { currentBlock }

-- |Set the current function.
setFunction :: String -> GenState ()
setFunction currentFunction =
    modify $ \s -> s { currentFunction }

-- |Set the terminator on the current block.
setTerm :: Named Terminator -> GenState ()
setTerm terminator =
    modifyBlock $ \block -> block { terminator = Just terminator }

-- |The type of an array of characters.
stringType :: AST_Type.Type
stringType = PointerType i8 $ AddrSpace 0

-- |The field index of SubRegion.currentAddress.
subRegionCurrentAddressField :: Integer
subRegionCurrentAddressField = 1

-- |The field index of SubRegion.next.
subRegionNextField :: Integer
subRegionNextField = 3

-- |The field index of SubRegion.size.
subRegionSizeField :: Integer
subRegionSizeField = 2

-- |The field index of SubRegion.start.
subRegionStartField :: Integer
subRegionStartField = 0

-- |The sub-region structure type.
subRegionType :: AST_Type.Type
subRegionType = NamedTypeReference $ Name "SubRegion"

-- |The sub-region pointer type.
subRegionPointerType :: AST_Type.Type
subRegionPointerType = PointerType subRegionType $ AddrSpace 0
