{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-|
Module: Driver
Description: The ion compiler driver.
-}

module Driver (drive) where

import Control.Monad (when)
import System.FilePath (takeBaseName)

import Gen (gen)
import Link (link)
import Parser (parse)
import SemanticAnalyzer (analyze)

-- |Drive the compilation of an ion program.
drive :: FilePath -> Bool -> Bool -> Bool -> IO ()
drive file showAst showLLVM debugMode = do
    result <- parse file
    case result of
         Left err -> print err
         Right ast -> do
            when showAst $
                print ast
            case analyze ast debugMode of
                 Left err -> print err
                 Right state -> do
                     modul <- gen state
                     link modul (takeBaseName file) showLLVM
