{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Environment
Description: Environment with scopes.
-}

module Environment
    ( Environment
    , Scope(..)
    , OperandTable
    , SymbolTable
    , addOperand
    , addSymbol
    , addSymbolsPattern
    , enterScope
    , getOperand
    , getSymbol
    , leaveScope
    ) where

import qualified Data.Map as Map

import LLVM.General.AST.Constant (Constant(GlobalReference))
import LLVM.General.AST.Operand (Operand(ConstantOperand))
import LLVM.General.AST.Name (Name(Name))
import LLVM.General.AST.Type (void)

import Ast ( Pattern(..)
           , PatternPos
           , Ty(..)
           )
import Position (WithPos(..))

-- |The environment is a stack of scope.
type Environment = [Scope]

-- |Scope Record.
data Scope = Scope
             { operandTable :: OperandTable
             , symbols :: SymbolTable
             }
             deriving (Show)

-- |The symbol table with Operand.
type OperandTable = Map.Map String Operand

-- |The symbol table.
type SymbolTable = Map.Map String Ty

-- |Add an operand to the operand table. Return the key if it is already in operand table.
addOperand :: String -> Operand -> Environment -> Environment
addOperand name operand (scope:scopes) =
    let operands = operandTable scope in
    case getOperand [scope] name of
         Just _ -> fail $ name ++ " is supposed to be unique."
         Nothing -> scope { operandTable = Map.insert name operand operands } : scopes
addOperand _ _ _ = fail "Supposed to have a global scope in environment."

-- |Add a symbol to the symbol table. Return the key if it is already in symbol table.
addSymbol :: String -> Ty -> Environment -> Either String Environment
addSymbol name symbolType (scope:scopes) =
    let symbolTable = symbols scope in
    case getSymbol [scope] name of
        Just _ -> Left name
        Nothing ->
            Right $ scope { symbols = Map.insert name symbolType symbolTable } : scopes
addSymbol _ _ _ = fail "Supposed to have a global scope in environment."

-- |Add symbols in pattern to the symbol table. Return a key if there is a symbol that is already in symbol table.
addSymbolsPattern :: PatternPos -> Ty -> Environment -> Either String Environment
addSymbolsPattern WithPos { item = Hole } _ env = Right env
addSymbolsPattern WithPos { item = Ident ident } typ env = addSymbol (item ident) typ env
addSymbolsPattern WithPos { item = TuplePattern patterns } (TupleTy types) env =
    iter patterns types (Right env)
    where iter (pat:pats) (typ:typs) (Right environment) =
              iter pats typs $ addSymbolsPattern pat typ environment
          iter _ _ (Left err) = Left err
          iter _ _ (Right environment) = Right environment
addSymbolsPattern _ _ _ = fail "should have tuple type"

-- |Enter a new scope. Push a new scope in Environment.
enterScope :: Environment -> Environment
enterScope env = Scope { operandTable = Map.empty
                       , symbols = Map.empty
                       } : env

-- |Get an operand from the operand table.
getOperand :: Environment -> String -> Maybe Operand
getOperand _ "globalProgramRegion" = Just $ ConstantOperand $ GlobalReference void $ Name "globalProgramRegion"
getOperand (scope:scopes) name =
    let operands = operandTable scope in
    case Map.lookup name operands of
         Just operand -> Just operand
         Nothing -> getOperand scopes name
getOperand _ _ = Nothing

-- |Get a symbol from the symbol table.
getSymbol :: Environment -> String -> Maybe Ty
getSymbol _ "globalProgramRegion" = Just $ Ty "Region"
getSymbol (scope:scopes) ident =
    let symbolTable = symbols scope in
    case Map.lookup ident symbolTable of
         Just typ -> Just typ
         Nothing -> getSymbol scopes ident
getSymbol _ _ = Nothing

-- |Leave a scope. Pop the current scope.
leaveScope :: Environment -> Environment
leaveScope (_:scopes) = scopes
leaveScope _ = fail "No scope to pop."
