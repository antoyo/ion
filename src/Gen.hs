{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Gen
Description: LLVM code generator.
-}

module Gen (gen) where

import Control.Monad (forM_, when, zipWithM)
import Control.Monad.State (execState, gets)
import Data.Char (ord)
import qualified Data.Map as Map
import Data.Maybe (fromJust, fromMaybe, isJust)

import qualified LLVM.General.AST as AST
import LLVM.General.AST ( Definition(GlobalDefinition, TypeDefinition)
                        , Parameter(Parameter)
                        , defaultModule
                        , functionDefaults
                        , moduleDefinitions
                        , moduleName
                        )
import LLVM.General.AST.AddrSpace (AddrSpace(AddrSpace))
import LLVM.General.AST.Constant (Constant(Array, Float, GlobalReference, Int, Null, Struct))
import qualified LLVM.General.AST.FloatingPointPredicate as FloatingPointPredicate
import qualified LLVM.General.AST.Constant as Constant
import LLVM.General.AST.Float
import LLVM.General.AST.Global (basicBlocks, globalVariableDefaults, initializer, isConstant, name, parameters, returnType, type')
import LLVM.General.AST.InlineAssembly (Dialect(IntelDialect))
import qualified LLVM.General.AST.InlineAssembly as InlineAssembly
import qualified LLVM.General.AST.Instruction as Instruction
import LLVM.General.AST.Instruction (Instruction(GetElementPtr), Named((:=), Do), Terminator(Ret))
import qualified LLVM.General.AST.IntegerPredicate as IntegerPredicate
import LLVM.General.AST.Name (Name(Name))
import LLVM.General.AST.Operand (Operand(ConstantOperand, LocalReference))
import qualified LLVM.General.AST.Type as AST_Type
import LLVM.General.AST.Type (Type(ArrayType, NamedTypeReference, PointerType, StructureType), double, i64, i8, i1, void)

import AnalyzerState (EnumValueTable, TypeTable)
import Ast ( BinaryOperation(..)
           , BinaryOperator(..)
           , Constant(..)
           , Definition(..)
           , EnumValue(..)
           , ExprEscape(..)
           , Expression'(..)
           , Expression
           , FunctionCall(..)
           , FunctionDefinition(..)
           , IfExpression(..)
           , InlineAssembly(..)
           , LetStatement(..)
           , Module
           , Pattern(..)
           , Statement(..)
           , StatementPos
           , StructField(..)
           , TupleField(..)
           , Ty(..)
           , Type(..)
           , TypeDefinition(..)
           , UnaryOperation(..)
           , UnaryOperator(..)
           , WithType(..)
           )
import qualified Environment
import GenState
    ( CompilerFunction(..)
    , CompilerFunctions
    , GenRecord(..)
    , GenState
    , addBlock
    , addGlobal
    , addInstr
    , addOperand
    , addUniqueBlock
    , allocMemory
    , callInstr
    , clearBlocks
    , enterScope
    , genFieldStore
    , genGetRegion
    , getBlocks
    , getEnumValue
    , getOperand
    , getFieldPointer
    , getType
    , leaveScope
    , nextName
    , nextUnName
    , regionSize
    , regionType
    , setBlock
    , setFunction
    , setTerm
    , stringType
    , subRegionCurrentAddressField
    , subRegionNextField
    , subRegionSizeField
    , subRegionStartField
    , subRegionPointerType
    , subRegionType
    )
import Position (WithPos(..), dummyPos)
import SemanticAnalyzer (statementType, toTy)

-- |Create the global region (to be called by the main function only).
createGlobalRegion :: GenState ()
createGlobalRegion = do
    -- Fetch the program break (heap) address.
     region <- genGetRegion
     let programStartBreak = Name "programStartBreak"
         getBreakFunc = Right $ ConstantOperand $ GlobalReference void $ Name "getBreak"
         getBreakCall = callInstr getBreakFunc []
     addInstr $ programStartBreak := getBreakCall
     let programBreak = LocalReference i64 programStartBreak
     genFieldStore region i64 regionStartField programBreak
     genFieldStore region i64 regionCurrentAddressField programBreak

-- |Create a sub-region.
createSubRegion :: String -> GenState ()
createSubRegion funcName = do
    let name = "region$" ++ funcName
        subRegionName = Name name
    addInstr $ subRegionName := Instruction.Alloca subRegionType Nothing 0 []
    globalRegion <- genGetRegion
    let regionAllocFunc = Right $ ConstantOperand $ GlobalReference void $ Name "Region$alloc"
        size = ConstantOperand $ Int 64 regionSize
        regionAllocCall = callInstr regionAllocFunc [globalRegion, size]
        subRegion = LocalReference subRegionType subRegionName
    startName <- nextUnName
    addInstr $ startName := regionAllocCall
    let start = LocalReference i64 startName
    genFieldStore subRegion i64 subRegionStartField start
    genFieldStore subRegion i64 subRegionCurrentAddressField start
    genFieldStore subRegion i64 subRegionSizeField size
    genFieldStore subRegion subRegionPointerType subRegionNextField $ ConstantOperand $ Null subRegionPointerType
    addOperand name subRegion

-- |Destroy the global region (to be called for the main function only).
destroyGlobalRegion :: GenState ()
destroyGlobalRegion = do
    region <- genGetRegion
    let regionDestroyFunc = Right $ ConstantOperand $ GlobalReference void $ Name "Region$destroy"
        regionDestroyCall = callInstr regionDestroyFunc [region]
    addInstr $ Do regionDestroyCall
    addInstr $ Do exitCall
    where exitFunc = Right $ ConstantOperand $ GlobalReference void $ Name "exit"
          exitCode = ConstantOperand $ Int 64 0
          exitCall = callInstr exitFunc [exitCode]

destroySubRegion :: String -> GenState ()
destroySubRegion funcName = do
    let subRegion = LocalReference subRegionPointerType $ Name $ "region$" ++ funcName
        subRegionDestroyFunc = Right $ ConstantOperand $ GlobalReference void $ Name "SubRegion$destroy"
        subRegionDestroyCall = callInstr subRegionDestroyFunc [subRegion]
    addInstr $ Do subRegionDestroyCall

-- |Generate an executable.
gen :: (Module, EnumValueTable, Environment.Environment, TypeTable, CompilerFunctions) -> IO AST.Module
gen (definitions, enumValueTable, environment, typeTable, compilerFuncs) = do
    let zero = Int 64 0
        region = GlobalDefinition $
                 globalVariableDefaults
                 { name = Name "globalProgramRegion"
                 , initializer = Just $ Struct (Just $ Name "Region") False [zero, zero]
                 , type' = regionType
                 }
        initState = GenRecord
                  { blocks = Map.empty
                  , compilerFuncs
                  , currentBlock = Name "entry"
                  , currentFunction = "main"
                  , enumValueTable
                  , environment
                  , globals = [region]
                  , stringIndex = 0
                  , typeTable
                  , unNameIndex = 0
                  }
        state = execState (mapM genDefinition definitions) initState
    let modul = defaultModule
              { moduleDefinitions = globals state
              , moduleName = "ion"
              }
    return modul

-- |Generate an argument.
genArgument :: Expression -> GenState Operand
genArgument = genExpression

-- |Generate a definition.
genDefinition :: Ast.Definition -> GenState ()
genDefinition (Function FunctionDefinition { callEscapingFunctions, functionName, functionParameters, functionStatements, functionType }) =
    case item functionType of
         FunctionType funcType -> do
             -- Enter scope
             enterScope
             def <- addBlock "entry"
             setBlock def
             let types = init $ map item funcType
                 funcName = item functionName
                 name = Name funcName

             setFunction funcName

             when (funcName == "main") createGlobalRegion
             when callEscapingFunctions $ createSubRegion funcName

             parameters <- zipWithM genParameter params types
             returnType <- toLLVMType $ item $ last funcType
             let statements = itemNode functionStatements
             statementOperands <- mapM genStatement statements
             let operand = last statementOperands

             when callEscapingFunctions $ destroySubRegion funcName
             when (funcName == "main") destroyGlobalRegion

             setTerm $ Do $ Ret operand []
             basicBlocks <- getBlocks
             clearBlocks
             -- Leave scope
             leaveScope
             addGlobal $ GlobalDefinition $ functionDefaults
                 { name
                 , parameters = (parameters, False)
                 , returnType
                 , basicBlocks
                 }
         _ -> fail "wrong function type"
    where params = map item functionParameters
genDefinition (MethodDefinition _ _) = fail "Method should have been transformed to a function"
genDefinition (TypeDef typ (StructDefinition fields)) = do
    defFields <- toDef fields
    addGlobal $ TypeDefinition (Name $ item typ) $ Just defFields
    where
        toDef structFields = do
            innerTypeStructFields <- mapM (toInnerType . item . fieldType) structFields
            return $ StructureType False innerTypeStructFields
        toInnerType (FunctionType _) = error "function type not allowed in struct"
        toInnerType (NullableType ident) = return $ PointerType (NamedTypeReference (Name $ item ident)) $ AddrSpace 0
        toInnerType (TupleType tupleFields) = do
            innerTypeTupleFields <- mapM (toInnerType . item) tupleFields
            return $ StructureType False innerTypeTupleFields
        toInnerType (Type WithPos { item = "Bool" }) = return i1
        toInnerType (Type WithPos { item = "Char" }) = return i8
        toInnerType (Type WithPos { item = "CharPtr" }) = return stringType
        toInnerType (Type WithPos { item = "Double" }) = return double
        toInnerType (Type WithPos { item = "Int" }) = return i64
        toInnerType (Type ident) = do
            typ2 <- getType $ item ident
            case typ2 of
                Just (StructDefinition _) ->
                    return $ PointerType (NamedTypeReference (Name $ item ident)) $ AddrSpace 0
                Just (EnumType enumValues) ->
                    if length enumValues <= 2 then
                        return i1
                    else
                        return i64
                _ -> fail $ "unknown type (Ty) " ++ item ident
genDefinition (TypeDef _ _) =
    return ()

-- |Generate the instructions from an Expression.
genExpression :: Expression -> GenState Operand
-- |Generate the instructions for a function call.
genExpression WithType { itemType
                       , itemNode = WithPos { item = Call escape FunctionCall { callArguments, callName }}
                       } = do
    let name = item callName
    callArgs <- mapM genArgument callArguments
    funcs <- gets compilerFuncs
    llvmType <- toLLVMTy itemType
    case Map.lookup name funcs of
         Just func -> compilerFunctionLLVM func callArgs escape llvmType
         Nothing -> do
            result <- nextUnName
            operand <- getOperand name
            let func = Right operand
                call = callInstr func callArgs
            addInstr $ result := call
            return $ LocalReference llvmType result

-- |Generate the instructions for a UnaryOperation Expression with Type Double.
genExpression WithType { itemType = Ty "Double"
                       , itemNode = WithPos { item = UnOp (UnaryOperation unOperator expr) }
                       } = do
    operand <- genExpression expr
    let returnType = double
    case unOperator of
         UnaryCustom _ -> fail "custom operator should have been transformed to a function call"
         UnaryMinus -> do
             let zero = ConstantOperand $ Float $ Double 0.0
                 instruction = Instruction.FSub Instruction.NoFastMathFlags zero operand []
             genOperand instruction returnType
         UnaryPlus ->
             return operand

-- |Generate the instructions for a UnaryOperation Expression with Type Int.
genExpression WithType { itemType = Ty "Int"
                       , itemNode = WithPos { item = UnOp (UnaryOperation unOperator expr) }
                       } = do
    operand <- genExpression expr
    let returnType = i64
    case unOperator of
         UnaryCustom _ -> error "custom operator should have been transformed to a function call"
         UnaryMinus -> do
             let zero = ConstantOperand $ Int 64 0
                 instruction = Instruction.Sub False False zero operand []
             genOperand instruction returnType
         UnaryPlus ->
             return operand

-- |Generate the instructions for an Assign Expression.
genExpression WithType { itemNode = WithPos { item = BinOp (BinaryOperation Assign expr1 expr2) }
                       } =
    case expr1 of
          WithType { itemNode = WithPos { item = Field
                    TupleField { tupleName = WithPos { item = tupleName }, tupleIndex }}
                , itemType } -> do
              tuple <- getOperand tupleName
              llvmType <- toLLVMTy itemType
              value <- genExpression expr2
              genFieldStore tuple llvmType tupleIndex value
              -- NOTE: return void value for field assignation statement.
              return $ ConstantOperand $ Int 64 0
          _ -> fail "should be a field on the left side of an assignment"

-- |Generate the instructions for a BinaryOperation Expression with type Int.
genExpression WithType { itemType = Ty "Bool"
                       , itemNode = WithPos { item = BinOp (BinaryOperation binOperator expr1 expr2) }
                       } = do
    (operand1, operand2) <- genOperands expr1 expr2
    let operandType = itemType expr1
    isEnum <- isEnumType operandType
    let returnType = i1
        instruction =
            case operandType of
                Ty "Double" ->
                    case binOperator of
                        CustomOper _ -> error "custom operator should have been transformed to a function call"
                        Equal -> Instruction.FCmp FloatingPointPredicate.OEQ
                        Greater -> Instruction.FCmp FloatingPointPredicate.OGT
                        GreaterEqual -> Instruction.FCmp FloatingPointPredicate.OGE
                        Less -> Instruction.FCmp FloatingPointPredicate.OLT
                        LessEqual -> Instruction.FCmp FloatingPointPredicate.OLE
                        NotEqual -> Instruction.FCmp FloatingPointPredicate.ONE
                        _ -> error $ "operator " ++ show binOperator ++ " doesn't return an Bool."
                Ty typ | typ == "Char" || typ == "Int" || typ == "Bool" || isEnum ->
                    case binOperator of
                        CustomOper _ -> error "custom operator should have been transformed to a function call"
                        Equal -> Instruction.ICmp IntegerPredicate.EQ
                        Greater -> Instruction.ICmp IntegerPredicate.SGT
                        GreaterEqual -> Instruction.ICmp IntegerPredicate.SGE
                        Less -> Instruction.ICmp IntegerPredicate.SLT
                        LessEqual -> Instruction.ICmp IntegerPredicate.SLE
                        NotEqual -> Instruction.ICmp IntegerPredicate.NE
                        _ -> error $ "operator " ++ show binOperator ++ " doesn't return an Bool."
                _ -> error "operand type is supposed to be a Bool, a Char, a Double, an enum type or an Int."
        fullInstruction = instruction operand1 operand2 []
    genOperand fullInstruction returnType
    where isEnumType typ1 =
              case typ1 of
                 Ty name -> do
                     maybeTypeDefinition <- getType name
                     case maybeTypeDefinition of
                         Just (EnumType _) -> return True
                         _ -> return False
                 _ -> return False

-- |Generate the instructions for a BinaryOperation Expression with type Double.
genExpression WithType { itemType = Ty "Double"
                       , itemNode = WithPos { item = BinOp (BinaryOperation binOperator expr1 expr2) }
                       } = do
    (operand1, operand2) <- genOperands expr1 expr2
    let returnType = double
        instruction =
            case binOperator of
                CustomOper _ -> error "custom operator should have been transformed to a function call"
                Divide -> Instruction.FDiv Instruction.NoFastMathFlags
                Minus -> Instruction.FSub Instruction.NoFastMathFlags
                Plus -> Instruction.FAdd Instruction.NoFastMathFlags
                Times -> Instruction.FMul Instruction.NoFastMathFlags
                _ -> error $ "operator " ++ show binOperator ++ " doesn't return a Double."
        fullInstruction = instruction operand1 operand2 []
    genOperand fullInstruction returnType

-- |Generate the instructions for a BinaryOperation Expression with type Int.
genExpression WithType { itemType = Ty "Int"
                       , itemNode = WithPos { item = BinOp (BinaryOperation binOperator expr1 expr2) }
                       } = do
    (operand1, operand2) <- genOperands expr1 expr2
    let returnType = i64
        instruction =
            case binOperator of
                CustomOper _ -> error "custom operator should have been transformed to a function call"
                Divide -> Instruction.SDiv False
                Minus -> Instruction.Sub False False
                Modulo -> Instruction.SRem
                Plus -> Instruction.Add False False
                Times -> Instruction.Mul False False
                _ -> error $ "operator " ++ show binOperator ++ " doesn't return an Int."
        fullInstruction = instruction operand1 operand2 []
    genOperand fullInstruction returnType

-- |Generate the instructions for an IfExpression.
genExpression WithType { itemType
                       , itemNode = WithPos { item = If IfExpression { condition, trueStatements, elseStatements } }
                       } = do
    returnType <- toLLVMTy itemType
    -- Generate the condition.
    cond <- genExpression condition
    currentBlockName <- gets currentBlock
    -- Add blocks
    ifThen <- addUniqueBlock "if.then"
    -- Else block is optional.
    ifElse <- case elseStatements of
                   Just _ -> do
                       temp <- addUniqueBlock "if.else"
                       return $ Just temp
                   Nothing -> return Nothing
    ifExit <- addUniqueBlock "if.exit"
    -- Add condBr.
    setBlock currentBlockName
    let cbr = Instruction.CondBr cond ifThen (fromMaybe ifExit ifElse) []
    setTerm $ Do cbr
    result <- nextUnName
    -- Generate the if.then branch.
    setBlock ifThen
    -- Enter scope
    enterScope
    trueTypedstatementOperands <- mapM genStatement trueStatements
    let trueTypedOperand = last trueTypedstatementOperands
    setTerm $ Do $ Instruction.Br ifExit []
    -- Leave scope
    leaveScope
    -- The currentBlock can change when we generate the if.then block.
    ifThen2 <- gets currentBlock
    -- Generate the if.else branch if exists.
    -- Enter scope
    enterScope
    elseTypedOperand <- case elseStatements of
                             Just statements -> do
                                 setBlock $ fromJust ifElse
                                 operands <- mapM genStatement statements
                                 let operand = last operands
                                 setTerm $ Do $ Instruction.Br ifExit []
                                 return $ Just operand
                             Nothing -> return Nothing
    -- Leave scope
    leaveScope
    -- The currentBlock can change when we generate the if.else block.
    ifElse2 <- case elseTypedOperand of
                    Just _ -> do
                        temp <- gets currentBlock
                        return $ Just temp
                    Nothing -> return Nothing
    -- Change the currentBlock for if.exit.
    setBlock ifExit
    -- Generate Phi Instruction for returnType other than void.
    when (isJust trueTypedOperand) $ do
        phi <- case elseTypedOperand of
                    Just elseOperand -> return $ Instruction.Phi returnType
                            [(fromJust trueTypedOperand, ifThen2), (fromJust elseOperand, fromJust ifElse2)] []
                    Nothing -> return $ Instruction.Phi returnType [(fromJust trueTypedOperand, ifThen2)] []
        addInstr $ result := phi
        return ()
    return $ LocalReference returnType result

-- |Generate the instructions for accessing a constant value.
genExpression WithType { itemNode = WithPos { item = Const Constant { constantName } }} =
    getOperand $ item constantName

-- |Generate the instructions for an Enum Expression.
genExpression WithType { itemNode = WithPos { item = EnumExpression ident } } = do
    enumV <- getEnumValue (item ident)
    llvmType <- toLLVMTy $ Ty $ item $ enumTy enumV
    let size = if llvmType == i1 then
                   1
               else
                   64
    return $ ConstantOperand $ Int size (enumValue enumV)

-- |Generate the instractions for a Char litteral.
genExpression WithType { itemNode = WithPos { item = CharLiteral char } } =
    return $ ConstantOperand $ Int 8 $ toInteger $ ord char

-- |Generate the instractions for a Double litteral.
genExpression WithType { itemNode = WithPos { item = DoubleLiteral value } } =
    return $ ConstantOperand $ Float $ Double value

-- |Generate the instructions for an Int literal.
genExpression WithType { itemNode = WithPos { item = IntegerLiteral int } } =
    return $ ConstantOperand $ Int 64 int

-- |Generate the instructions for a String literal.
genExpression WithType { itemNode = WithPos { item = GlobalString string } } = do
    globalName <- nextName "string"
    let stringLen = fromIntegral (length string) + 1 -- NOTE: plus 1 for the null byte at the end.
    addGlobal $ GlobalDefinition $ globalVariableDefaults
                { name = globalName
                , isConstant = True
                , initializer = Just $ stringToArray string
                , type' = ArrayType stringLen i8
                }
    name <- nextUnName
    let array = ConstantOperand $ GlobalReference i8 globalName
        index = ConstantOperand $ Int 64 0
        stringAddress = GetElementPtr True array [index, index] []
    addInstr $ name := stringAddress
    return $ LocalReference stringType name

-- |Generate an inline assembly expression.
genExpression WithType { itemNode = WithPos { item =
        InlineAsm Ast.InlineAssembly { assembly, inputArguments, inputConstraints, outputConstraints }}} = do
    result <- nextUnName
    callArgs <- mapM genArgument inputArguments
    paramTypes <- mapM (toLLVMTy . itemType) inputArguments
    let returnType = if null outputConstraints then void else i64
        separator = if null outputConstraints || null inputConstraints then "" else ", "
        constraints = outputConstraints ++ separator ++ inputConstraints
        inlineAsm = Left $ InlineAssembly.InlineAssembly (AST_Type.FunctionType returnType paramTypes False) assembly constraints True False IntelDialect
        call = callInstr inlineAsm callArgs
        llvmType = i64
    addInstr $ result := call
    return $ LocalReference llvmType result

-- |Generate an empty tuple expression.
genExpression  WithType { itemNode = WithPos { item =
            Tuple _ _ [] },
        itemType = structType } = do
    typ <- toLLVMTy structType
    return $ ConstantOperand $ Null typ

-- |Generate a tuple expression.
genExpression WithType { itemNode = WithPos { item =
            Tuple structType escape expressions },
        itemType = tupleType } = do
    let types = map itemType expressions
    llvmTypes <- mapM toLLVMTy types
    let llvmStructType =
            case structType of
                 Just name -> NamedTypeReference $ Name name
                 Nothing -> StructureType False llvmTypes
    let llvmPointerStructType = PointerType llvmStructType $ AddrSpace 0
    exprs <- mapM genExpression expressions
    tupleName <- nextUnName
    let tuple = LocalReference llvmPointerStructType tupleName

    if escape == Escape then do
        structSize <- typeSize tupleType
        let sizeOperand = ConstantOperand $ Int 64 structSize
            region = "region$0"
        address <- allocMemory sizeOperand region
        addInstr $ tupleName := Instruction.IntToPtr address llvmPointerStructType []
    else
        addInstr $ tupleName := Instruction.Alloca llvmStructType Nothing 0 []

    forM_ (zip3 [0..] exprs llvmTypes) $ \(index, expr, fieldType) -> do
        fieldName <- nextUnName
        let index1 = ConstantOperand $ Int 64 0
            index2 = ConstantOperand $ Int 32 index
        addInstr $ fieldName := Instruction.GetElementPtr True tuple [index1, index2] []
        let field = LocalReference fieldType fieldName
        addInstr $ Do $ Instruction.Store False field expr Nothing 0 []
    return tuple

-- |Generate a tuple field access.
genExpression WithType { itemNode = WithPos { item =
            Field TupleField { tupleName, tupleIndex }},
        itemType } = do
    operand <- getOperand $ item tupleName
    llvmType <- toLLVMTy itemType
    genFieldLoad operand llvmType tupleIndex

genExpression WithType { itemNode = WithPos { item = StringLiteral _ } } = fail "String literal should have been converted to struct"

genExpression expr = fail $ "invalid expression: " ++ show (item $ itemNode expr)

-- |Generate the code to access a LLVM tuple's field.
genFieldLoad :: Operand -> AST_Type.Type -> Integer -> GenState Operand
genFieldLoad tuple llvmType index = do
    field <- getFieldPointer tuple llvmType index
    value <- nextUnName
    addInstr $ value := Instruction.Load False field Nothing 0 []
    return $ LocalReference llvmType value

-- |Generate an operand
genOperand :: Instruction -> AST_Type.Type -> GenState Operand
genOperand instruction returnType = do
    result <- nextUnName
    addInstr $ result := instruction
    return $ LocalReference returnType result

-- |Generate operands.
genOperands :: Expression -> Expression -> GenState (Operand, Operand)
genOperands exp1 exp2 = do
    op1 <- genExpression exp1
    op2 <- genExpression exp2
    return (op1, op2)

-- |Generate a parameter.
genParameter :: Pattern -> Ast.Type -> GenState Parameter
genParameter (Ident paramIdent) typ = do
    let name = Name (item paramIdent)
    paramType <- toLLVMType typ
    addOperand (item paramIdent) $ LocalReference paramType name
    return $ Parameter paramType name []
genParameter _ _ = fail "Not supposed to have pattern to generate in a parameter."

-- |Generate the basic block instructions from Statements.
genStatement :: StatementPos -> GenState (Maybe Operand)

-- |Generate an expression statement.
genStatement statement@WithPos { item = Expr expr } = do
    operand <- genExpression expr
    return $
        if statementType statement == TupleTy [] then
            Nothing
        else
            Just operand

-- |Generate a let statement.
genStatement WithPos { item =
        Let LetStatement { letStatementPattern, letStatementValue, letStatementType } } = do
    expr <- genExpression letStatementValue
    case item letStatementPattern of
        Hole -> return Nothing
        Ident letStatementName -> do
            addOperand (item letStatementName) expr
            return Nothing
        TuplePattern patterns ->
            case item letStatementType of
                 TupleType types -> do
                    addOperandsPatterns expr patterns types
                    return Nothing
                 _ -> fail "Must be TupleTy."
    where addOperandsPatterns expr patterns types =
            forM_ (zip (zip patterns types) [0..]) $
                \((pat, typ), index) -> addOperandsPattern expr pat typ index
          addOperandsPattern expr pat typ index =
            case item pat of
                 Hole -> return ()
                 Ident ident -> do
                     llvmType <- toLLVMType $ item typ
                     operand <- genFieldLoad expr llvmType index
                     addOperand (item ident) operand
                     return ()
                 TuplePattern patterns -> do
                     llvmType <- toLLVMType $ item typ
                     operandExpr <- genFieldLoad expr llvmType index
                     case item typ of
                          TupleType types ->
                              addOperandsPatterns operandExpr patterns types
                          _ -> fail "Must be TupleTy"
                     return ()

-- |The structure field containing the region current address.
regionCurrentAddressField :: Integer
regionCurrentAddressField = 1

-- |The structure field containing the region start address.
regionStartField :: Integer
regionStartField = 0

-- |Convert a String to an LLVM Array constant.
stringToArray :: String -> Constant.Constant
stringToArray string =
    Array i8 chars
    where toI8 char = Int 8 $ fromIntegral $ ord char
          nullChar = Int 8 0
          chars = map toI8 string ++ [nullChar]

-- |Convert a type to its LLVM representation.
toLLVMTy :: Ty -> GenState AST_Type.Type
toLLVMTy (Ty "Bool") = return i1
toLLVMTy (Ty "Char") = return i8
toLLVMTy (Ty "CharPtr") = return stringType
toLLVMTy (Ty "Double") = return double
toLLVMTy (Ty "Int") = return i64
toLLVMTy (TupleTy []) = return void
toLLVMTy (TupleTy types) = do
    typs <- mapM toLLVMTy types
    return $ PointerType (StructureType False typs) $ AddrSpace 0
toLLVMTy (Ty ty) = do
    typ <- getType ty
    case typ of
        Just (StructDefinition _) ->
            return $ PointerType (NamedTypeReference $ Name ty) $ AddrSpace 0
        Just (EnumType enumValues) ->
            if length enumValues <= 2 then
                return i1
            else
                return i64
        _ -> fail $ "unknown type (Ty) " ++ ty
toLLVMTy (FuncTy types) = toLLVMTy $ last types
toLLVMTy (NullableTy typ) = toLLVMTy $ Ty typ
toLLVMTy typ = fail $ "unknown type (Ty) " ++ show typ

-- |Convert a type to its LLVM representation.
toLLVMType :: Ast.Type -> GenState AST_Type.Type
toLLVMType = toLLVMTy . toTy . dummyPos

-- |Get the size of a type.
typeSize :: Ty -> GenState Integer
typeSize (Ty "Bool") = return 1
typeSize (Ty "Char") = return 1
typeSize (Ty "Double") = return 8
typeSize (Ty "Int") = return 8
typeSize (Ty "String") = return $ 8 + 8 -- String has two Int fields.
typeSize (NullableTy _) = return 8
typeSize (TupleTy types) = do
    sizes <- mapM typeSize types
    return $ sum sizes
typeSize (Ty ty) = do
    typ <- getType ty
    case typ of
        Just (StructDefinition fields) -> do
            sizes <- mapM (typeSize . toTy . fieldType) fields
            return $ sum sizes
        Just (EnumType _) -> return 8
        _ -> fail ("Cannot find the size of type " ++ show ty)
typeSize typ = fail $ "typeSize: unknown type " ++ show typ
