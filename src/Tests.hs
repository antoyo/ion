{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

import Control.Monad (forM_)
import System.Directory (getDirectoryContents)
import System.Exit (exitFailure, exitSuccess)
import System.FilePath ((</>), dropExtension, takeExtension)
import System.IO (Handle, hGetContents, hSetBinaryMode)
import System.Process (rawSystem, runInteractiveCommand, runInteractiveProcess)

import Test.HUnit (Counts(Counts), Test(TestCase), assertEqual, runTestTT, showCounts)

import Driver (drive)

assertHandle :: String -> Handle -> String -> IO ()
assertHandle message handle expected = do
    hSetBinaryMode handle False
    result <- hGetContents handle
    assertEqual message expected $ removeEscapes result

assertHandles :: String -> Handle -> Handle -> String -> IO ()
assertHandles message handle1 handle2 expected = do
    hSetBinaryMode handle1 False
    hSetBinaryMode handle2 False
    result1 <- hGetContents handle1
    result2 <- hGetContents handle2
    assertEqual message expected $ removeEscapes $ result1 ++ result2

ext :: FilePath -> FilePath -> Bool
ext extension = (== extension) . takeExtension

getTests :: String -> IO [(String, String)]
getTests directory = do
    files <- getDirectoryContents directory
    let inputs = map dropExtension $ filter (ext ".ion") files
        outputs = map dropExtension inputs
    return $ zip inputs outputs

removeEscapes :: String -> String
removeEscapes ('\ESC' : rest) = removeEscapes $ tail $ dropWhile (/= 'm') rest
removeEscapes (c : rest) = c : removeEscapes rest
removeEscapes [] = []

testFail :: String -> FilePath -> IO ()
testFail name expectedOutput = do
    let ionFile = name ++ ".ion"
        errorDir = "tests" </> "errors"
        testFile = errorDir </> ionFile
        outputFile = errorDir </> expectedOutput
    expected <- readFile outputFile
    (_, output, _, _) <- runInteractiveProcess "./dist/build/ion/ion" [testFile] Nothing Nothing
    assertHandle ionFile output expected

testProg :: String -> FilePath -> IO ()
testProg name expectedOutput = do
    let ionFile = name ++ ".ion"
        testFile = "tests" </> ionFile
        executable = "build" </> name
        outputFile = "tests" </> expectedOutput
    expected <- readFile outputFile
    drive testFile False False True
    (_, output, _, _) <- runInteractiveCommand executable
    assertHandle ionFile output expected
    (_, valgrindOutput, valgrindError, _) <- runInteractiveProcess "valgrind" ["-q", executable] Nothing Nothing
    assertHandles ionFile valgrindOutput valgrindError expected

tests :: Test
tests = TestCase $ do
    inputOutputs <- getTests "tests"
    forM_ inputOutputs $ uncurry testProg

    errorInputOutputs <- getTests "tests/errors"
    forM_ errorInputOutputs $ uncurry testFail

main :: IO ()
main = do
    _ <- rawSystem "cabal" ["build"]
    cs@(Counts _ _ errs fails) <- runTestTT tests
    putStrLn (showCounts cs)
    if errs > 0 || fails > 0 then
        exitFailure
    else
        exitSuccess
