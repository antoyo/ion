{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Ast
Description: The ion ast.
-}

module Ast
    ( BinaryOperation(..)
    , BinaryOperator(..)
    , Constant(..)
    , Definition(..)
    , EnumValue(..)
    , Escape(..)
    , ExprEscape(..)
    , Expression'(..)
    , Expression
    , FunctionCall(..)
    , FunctionDefinition(..)
    , Identifier
    , IfExpression(..)
    , InlineAssembly(..)
    , LetStatement(..)
    , Module
    , Pattern(..)
    , PatternPos
    , Statement(..)
    , StatementPos
    , StructField(..)
    , StructFieldValue(..)
    , StructFuncCall(..)
    , TupleField(..)
    , Ty(..)
    , Type(..)
    , TypeDefinition(..)
    , TypePos
    , UnaryOperation(..)
    , UnaryOperator(..)
    , WithType(..)
    , untyped
    , withPosUntyped
    ) where

import Text.Parsec.Pos (SourcePos)

import Position (WithPos(..))

-- |Binary operation.
data BinaryOperation = BinaryOperation BinaryOperator Expression Expression
    deriving (Show)

-- |Binary operators.
data BinaryOperator
    = And
    | Assign
    | CustomOper Identifier
    | Divide
    | Equal
    | Greater
    | GreaterEqual
    | Less
    | LessEqual
    | Minus
    | Modulo
    | NotEqual
    | Or
    | Plus
    | Times
    deriving (Show)

-- |A function call without parameter is transformed into a constant.
data Constant =
    Constant
    { constantName :: Identifier
    }
    deriving (Show)

-- |Module definitions.
data Definition
    = Function FunctionDefinition
    | MethodDefinition Identifier FunctionDefinition
    | TypeDef Identifier TypeDefinition
    deriving (Show)

-- |An enum value is an element of an enum type.
data EnumValue =
    EnumValue
    { enumIdent :: Identifier
    , enumValue :: Integer
    , enumTy :: Identifier
    }
    deriving (Show)

-- |An escape is an expression that outlive its function scope.
data Escape
    = EscapeConst String
    | EscapeField String Integer
    deriving (Show)

-- |The escape scope determines if a variable escape and its scope.
data ExprEscape
    = Escape
    | NoEscape
    deriving (Eq, Show)

-- |Expression.
data Expression'
    = BinOp BinaryOperation
    | Call ExprEscape FunctionCall
    | CharLiteral Char
    | Const Constant
    | DoubleLiteral Double
    | EnumExpression Identifier
    | Field TupleField
    | GlobalString String
    | If IfExpression
    | InlineAsm InlineAssembly
    | IntegerLiteral Integer
    | MethodCall StructFuncCall
    | StringLiteral String
    | StructExpression Identifier [StructFieldValue]
    | Tuple (Maybe String) ExprEscape [Expression]
    | UnOp UnaryOperation
    deriving (Show)

type Expression = WithType (WithPos Expression')

-- |A function call expression.
data FunctionCall =
    FunctionCall
    { callArguments :: [Expression]
    , callName :: Identifier
    }
    deriving (Show)

-- |Function definition.
data FunctionDefinition =
    FunctionDefinition
    { callEscapingFunctions :: Bool
    , escapingParameters :: [Int]
    , functionEscapes :: [Escape]
    , functionHasEscapes :: Bool
    , functionName :: Identifier
    , functionParameters :: [PatternPos]
    , functionStatements :: WithType [StatementPos]
    , functionType :: TypePos
    }
    deriving (Show)

type Identifier = WithPos String

-- |An if expression.
data IfExpression =
    IfExpression
    { condition :: Expression
    , elseStatements :: Maybe [StatementPos]
    , trueStatements :: [StatementPos]
    }
    deriving (Show)

-- |A let statement.
data LetStatement =
    LetStatement
    { letStatementPattern :: PatternPos
    , letStatementType :: TypePos
    , letStatementValue :: Expression
    }
    deriving (Show)

-- |Inline assembly expression.
data InlineAssembly =
    InlineAssembly
    { assembly :: String
    , inputArguments :: [Expression]
    , inputConstraints :: String
    , outputConstraints :: String
    }
    deriving (Show)

-- |A module is a collection of definitions.
type Module = [Definition]

-- |A Pattern is either an identifier or a Tuple.
data Pattern
    = Hole
    | Ident Identifier
    | TuplePattern [PatternPos]
    deriving (Show)

type PatternPos = WithPos Pattern

-- |A Statement is either a constant declaration or expression.
data Statement
    = Expr Expression
    | Let LetStatement
    deriving (Show)

type StatementPos = WithPos Statement

-- |A structure field definition.
data StructField = StructField
                 { fieldIndex :: Integer
                 , fieldMutable :: Bool
                 , fieldName :: Identifier
                 , fieldType :: TypePos
                 }
                 deriving (Show)

-- |A structure field name and value.
data StructFieldValue = StructFieldValue
                      { fieldValueName :: Identifier
                      , fieldValue :: Expression
                      }
                      deriving (Show)

-- |A method call.
data StructFuncCall = StructFuncCall
                    { methodCallArguments :: [Expression]
                    , methodCallName :: Identifier
                    , structName :: Identifier
                    }
                    deriving (Show)

-- |A tuple field index (or a struct field access).
data TupleField =
    TupleField
    { tupleName :: Identifier
    , tupleIndex :: Integer
    , structFieldMutable :: Bool
    , structFieldName :: Identifier
    , structFieldType :: Ty
    }
    deriving (Show)

-- |Type of an expression (without a position).
data Ty
    = AnyType
    | FuncTy [Ty]
    | NullableTy String
    | TupleTy [Ty]
    | Ty String
    | UnTy
    deriving (Eq, Show)

-- |Type of an expression.
data Type
    = FunctionType [TypePos]
    | NullableType Identifier
    | TupleType [TypePos]
    | Type Identifier
    deriving (Show)

type TypePos = WithPos Type

-- |A type definition is either an enumeration, a tuple or a struct.
data TypeDefinition
    = EnumType [EnumValue]
    | StructDefinition [StructField]
    | TypeSynonym TypePos
    deriving (Show)

-- |Unary operation.
data UnaryOperation = UnaryOperation UnaryOperator Expression
    deriving (Show)

-- |Unary operators.
data UnaryOperator
    = UnaryCustom Identifier
    | UnaryMinus
    | UnaryPlus
    deriving (Show)

data WithType a =
    WithType
    { itemNode :: a
    , itemType :: Ty
    }
    deriving (Show)

-- |Wrap a node without a type.
untyped :: a -> WithType a
untyped itemNode = WithType { itemNode, itemType = UnTy }

-- |Create a new untyped WithPos.
withPosUntyped :: SourcePos -> a -> WithType (WithPos a)
withPosUntyped position item = untyped WithPos
    { item
    , position
    }
