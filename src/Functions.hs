{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-|
Module: Functions
Description: The functions implemented in the compiler.
-}

module Functions (compilerEscapingFunctions, compilerFunctionNames, compilerFunctions, ionDebug) where

import Control.Monad.State (gets)
import qualified Data.Map as Map

import LLVM.General.AST.AddrSpace (AddrSpace(AddrSpace))
import LLVM.General.AST.Constant (Constant(GlobalReference, Int, Null))
import LLVM.General.AST.Instruction (Named((:=), Do))
import qualified LLVM.General.AST.Instruction as Instruction
import qualified LLVM.General.AST.IntegerPredicate as IntegerPredicate
import LLVM.General.AST.Name (Name(Name))
import LLVM.General.AST.Operand (Operand(ConstantOperand, LocalReference))
import LLVM.General.AST.Type (Type(PointerType), i1, i64, i8, void)

import Ast (ExprEscape(..), Ty(..))
import GenState
    ( CompilerFunction(..)
    , CompilerFunctions
    , GenRecord(..)
    , allocMemory
    , addInstr
    , callInstr
    , genFieldStore
    , genGetRegion
    , nextUnName
    , regionSize
    , stringType
    , subRegionCurrentAddressField
    {-, subRegionNextField-}
    , subRegionSizeField
    , subRegionStartField
    , subRegionPointerType
    )

-- |List the escaping compiler functions.
compilerEscapingFunctions :: [String]
compilerEscapingFunctions =
    Map.keys $ Map.filter ((== True) . compilerFunctionEscape) compilerFunctions

-- |List the compiler functions.
compilerFunctionNames :: [String]
compilerFunctionNames =
    "ionDebug" : Map.keys (Map.filter ((== False) . compilerFunctionEscape) compilerFunctions)

-- |The list of all compiler functions.
compilerFunctions :: CompilerFunctions
compilerFunctions =
    Map.fromList
    [ ("charToPointer", charToPointer)
    , ("chr", ionChr)
    , ("isNull", isNull)
    , ("null", ionNull)
    , ("ord", ionOrd)
    , ("newSubRegion", newSubRegion)
    , ("stringToPointer", stringToPointer)
    , ("unsafeGet", unsafeGet)
    , ("unsafeNew", unsafeNew)
    , ("unsafeSet", unsafeSet)
    ]

-- |Get the address of a Char.
charToPointer :: CompilerFunction
charToPointer =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ -> do
        memoryName <- nextUnName
        addInstr $ memoryName := Instruction.Alloca i8 Nothing 0 []
        let memory = LocalReference (PointerType i8 $ AddrSpace 0) memoryName
        pointer <- nextUnName
        let arg = head args
        addInstr $ Do $ Instruction.Store False memory arg Nothing 0 []
        addInstr $ pointer := Instruction.GetElementPtr True memory [ConstantOperand $ Int 64 0] []
        result <- nextUnName
        addInstr $ result := Instruction.PtrToInt (LocalReference (PointerType i8 $ AddrSpace 0) pointer) i64 []
        return $ LocalReference i64 result
    , compilerFunctionType = [Ty "Char", Ty "Int"]
    }

-- |Convert an Int to a Char.
ionChr :: CompilerFunction
ionChr =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ -> do
        result <- nextUnName
        let arg = head args
        addInstr $ result := Instruction.Trunc arg i8 []
        return $ LocalReference i8 result
    , compilerFunctionType = [Ty "Int", Ty "Char"]
    }

-- |Returns whether the program was compiled in debug mode or not.
ionDebug :: Bool -> CompilerFunction
ionDebug debug =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \_ _ _ -> do
        let constant = ConstantOperand $ Int 1 $
                if debug then
                    1
                else
                    0
        return constant
    , compilerFunctionType = [Ty "Bool"]
    }

-- |Return a null pointer.
ionNull :: CompilerFunction
ionNull =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \_ _ typ ->
        return $ ConstantOperand $ Null typ
    , compilerFunctionType = [AnyType]
    }

-- |Convert a Char to an Int.
ionOrd :: CompilerFunction
ionOrd =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ -> do
        result <- nextUnName
        let arg = head args
        addInstr $ result := Instruction.ZExt arg i64 []
        return $ LocalReference i64 result
    , compilerFunctionType = [Ty "Char", Ty "Int"]
    }

-- |Return a null pointer.
isNull :: CompilerFunction
isNull =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ -> do
        let arg = head args
        value <- nextUnName
        addInstr $ value := Instruction.PtrToInt arg i64 []
        result <- nextUnName
        addInstr $ result := Instruction.ICmp IntegerPredicate.EQ (ConstantOperand $ Int 64 0) (LocalReference i64 value) []
        return $ LocalReference i1 result
    , compilerFunctionType = [AnyType, Ty "Bool"]
    }

-- |Alloc memory.
newSubRegion :: CompilerFunction
newSubRegion =
    CompilerFunction
    { compilerFunctionEscape = True
    , compilerFunctionLLVM = \args _ _ ->
        case args of
             [] -> do
                globalRegion <- genGetRegion
                let regionAllocFunc = Right $ ConstantOperand $ GlobalReference void $ Name "Region$alloc"
                    size = ConstantOperand $ Int 64 regionSize
                    regionAllocCall = callInstr regionAllocFunc [globalRegion, size]
                startName <- nextUnName
                subRegionName <- nextUnName
                let start = LocalReference i64 startName
                    subRegion = LocalReference subRegionPointerType subRegionName
                addInstr $ startName := regionAllocCall
                addInstr $ subRegionName := Instruction.IntToPtr start subRegionPointerType []
                genFieldStore subRegion i64 subRegionStartField start
                genFieldStore subRegion i64 subRegionCurrentAddressField start
                genFieldStore subRegion i64 subRegionSizeField size
                {-genFieldStore subRegion subRegionPointerType subRegionNextField $ ConstantOperand $ Null subRegionPointerType-}
                return subRegion
             _ -> fail "wrong call to newSubRegion"
    , compilerFunctionType = [Ty "Int", Ty "CharPtr"]
    }

-- |Convert a CharPtr to an Int (pointer).
stringToPointer :: CompilerFunction
stringToPointer =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ -> do
        -- Cast a pointer to an int.
        stringPtr <- nextUnName
        let arg = head args
        addInstr $ stringPtr := Instruction.GetElementPtr True arg [ConstantOperand $ Int 64 0] []
        result <- nextUnName
        addInstr $ result := Instruction.PtrToInt (LocalReference (PointerType i8 $ AddrSpace 0) stringPtr) i64 []
        return $ LocalReference i64 result
    , compilerFunctionType = [Ty "CharPtr", Ty "Int"]
    }

-- |Get an element from an array.
unsafeGet :: CompilerFunction
unsafeGet =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ ->
        case args of
             [pointer, index] -> do
                getAddress <- nextUnName
                addInstr $ getAddress := Instruction.GetElementPtr True pointer [index] []
                let addressOperand = LocalReference (PointerType i8 $ AddrSpace 0) getAddress
                char <- nextUnName
                addInstr $ char := Instruction.Load False addressOperand Nothing 0 []
                return $ LocalReference i8 char
             _ -> fail "wrong call to unsafeGet"
    , compilerFunctionType = [Ty "CharPtr", Ty "Int", Ty "Char"]
    }

-- |Alloc memory.
unsafeNew :: CompilerFunction
unsafeNew =
    CompilerFunction
    { compilerFunctionEscape = True
    , compilerFunctionLLVM = \args escape _ ->
        case args of
             [size] -> do
                 currentFunctionName <- gets currentFunction
                 let region =
                        case escape of
                             Escape -> "region$0"
                             NoEscape -> "region$" ++ currentFunctionName
                 memory <- allocMemory size region
                 result <- nextUnName
                 addInstr $ result := Instruction.IntToPtr memory stringType []
                 return $ LocalReference stringType result
             _ -> fail "wrong call to unsafeNew"
    , compilerFunctionType = [Ty "Int", Ty "CharPtr"]
    }

-- |Set an element in an array.
unsafeSet :: CompilerFunction
unsafeSet =
    CompilerFunction
    { compilerFunctionEscape = False
    , compilerFunctionLLVM = \args _ _ ->
        case args of
             [pointer, index, value] -> do
                address <- nextUnName
                addInstr $ address := Instruction.GetElementPtr True pointer [index] []
                let addressOperand = LocalReference (PointerType i8 $ AddrSpace 0) address
                addInstr $ Do $ Instruction.Store False addressOperand value Nothing 0 []
                return $ ConstantOperand $ Int 64 0
             _ -> fail "wrong call to unsafeSet"
    , compilerFunctionType = [Ty "CharPtr", Ty "Int", Ty "Char", TupleTy []]
    }
