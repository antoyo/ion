{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-|
Module: Position
Description: A position to be associated with a token or an AST node.
-}

{-# LANGUAGE NamedFieldPuns #-}

module Position (WithPos(..), dummyPos, withPos) where

import Text.Parsec.Pos (SourcePos, initialPos)

-- |Associate a position to an item (AST node).
data WithPos a = WithPos
               { item :: a
               , position :: SourcePos
               }
               deriving (Show)

instance Functor WithPos where
    fmap f WithPos { item, position } = WithPos { item = f item, position }

-- |Wrap an item in a position.
dummyPos :: a -> WithPos a
dummyPos item = WithPos
    { item
    , position = initialPos ""
    }

-- |Create a new WithPos.
withPos :: SourcePos -> a -> WithPos a
withPos position item = WithPos
    { item
    , position
    }
