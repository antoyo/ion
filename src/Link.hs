{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-|
Module: Link
Description: Create an executable from LLVM IR.
-}

module Link (link) where

import Control.Monad (when)
import Control.Monad.Except (runExceptT)
import System.Directory (createDirectoryIfMissing)
import System.Process (rawSystem)

import LLVM.General.Analysis (verify)
import LLVM.General.AST (Module)
import LLVM.General.Context (withContext)
import LLVM.General.Module (File(File), moduleLLVMAssembly, writeObjectToFile, withModuleFromAST)
import LLVM.General.PassManager (PassSetSpec, defaultCuratedPassSetSpec, optLevel, runPassManager, withPassManager)
import LLVM.General.Target (initializeNativeTarget, withHostTargetMachine)

-- |Create an object and an executable files from the LLVM module.
link :: Module -> String -> Bool -> IO ()
link modul sourceFileBaseName showLLVM = do
    initializeNativeTarget
    createDirectoryIfMissing False "build"
    let filename = "build/" ++ sourceFileBaseName ++ ".o"
        file = File filename
    _ <- withContext $ \context -> do
        moduleResult <- runExceptT $ withModuleFromAST context modul $ \cppModule ->
            withPassManager passes $ \passManager -> do
                -- Check that the module is correct.
                result <- runExceptT $ verify cppModule

                -- Show LLVM assembly.
                when showLLVM $ do
                    assembly <- moduleLLVMAssembly cppModule
                    putStrLn assembly

                case result of
                     Left llvmError -> putStrLn llvmError
                     Right _ -> return ()

                -- Optimize the module.
                _ <- runPassManager passManager cppModule

                -- Compile the module.
                targetResult <- runExceptT $ withHostTargetMachine $ \targetMachine -> do
                    writeResult <- runExceptT $ writeObjectToFile targetMachine file cppModule
                    case writeResult of
                         Left llvmError -> putStrLn llvmError
                         Right _ -> createExecutable filename $ "build/" ++ sourceFileBaseName
                case targetResult of
                     Left llvmError -> putStrLn llvmError
                     Right _ -> return ()
        case moduleResult of
             Left llvmError -> putStrLn llvmError
             Right _ -> return ()
    return ()

-- |Call the linker to create an executable.
createExecutable :: FilePath -> String -> IO ()
createExecutable filename moduleName = do
    _ <- rawSystem "ld" ["-e", "main", "-dynamic-linker", "/lib64/ld-linux-x86-64.so.2", filename, "-o", moduleName]
    return ()

passes :: PassSetSpec
passes = defaultCuratedPassSetSpec { optLevel = Just 3 }
