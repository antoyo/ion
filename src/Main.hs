{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-|
Module: Main
Description: The ion compiler.
-}

{-# LANGUAGE DeriveDataTypeable #-}

import System.Console.CmdArgs (Data, Typeable, (&=), args, cmdArgs, def, help, summary, typ, typFile)

import Driver (drive)

version :: String
version = "0.0.1"

-- |The command-line arguments data type.
data Arguments = Arguments
               { astShow :: Bool
               , debug :: Bool
               , ionFile :: FilePath
               , llvmShow :: Bool
               }
               deriving (Data, Show, Typeable)

-- |The command-line arguments.
arguments :: Arguments
arguments = Arguments
          { ionFile = def &= args &= typFile
          , astShow = False &= typ "Bool" &= help "Show AST."
          , debug = False &= typ "Bool" &= help "Set the ionDebug variable."
          , llvmShow = False &= typ "Bool" &= help "Show LLVM code."
          }
          &=
          summary ("The ion compiler, version " ++ version)

main :: IO ()
main = do
    cliArgs <- cmdArgs arguments
    let mainFile = ionFile cliArgs
        showAST = astShow cliArgs
        debugMode = debug cliArgs
        showLLVM = llvmShow cliArgs
    drive mainFile showAST showLLVM debugMode
