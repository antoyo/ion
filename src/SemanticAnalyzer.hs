{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: SemanticAnalyzer
Description: The ion semantic analyzer.
-}

module SemanticAnalyzer (analyze, statementType, toTy) where

import Control.Monad (forM_, when, unless)
import Control.Monad.State (gets, modify)
import Control.Monad.Trans (lift)
import Control.Monad.Trans.State (execStateT, runStateT)
import Data.Maybe (catMaybes, fromJust, isJust, isNothing)
import Data.List (intercalate)
import qualified Data.Map as Map
import qualified Data.Set as Set

import LLVM.General.AST.Constant (Constant(GlobalReference))
import LLVM.General.AST.Operand (Operand(ConstantOperand))
import LLVM.General.AST.Name (Name(Name))
import LLVM.General.AST.Type (void)
import Text.Parsec.Error (Message(..), newErrorMessage)
import Text.Parsec.Pos (SourcePos)
import Text.PrettyPrint.ANSI.Leijen ((<+>), bold, red, text)

import AnalyzerState
    ( AnalyzerState
    , AnalyzerRecord(..)
    , EnumValueTable
    , Error
    , TypeTable
    , addEnumValue
    , addMethod
    , addType
    , getEnumValue
    , getEnv
    , getMethod
    , getType
    , modifyEnv
    , modifyEnvironment
    )
import Ast ( BinaryOperation(..)
           , BinaryOperator(..)
           , Constant(..)
           , Definition(..)
           , EnumValue(..)
           , ExprEscape(..)
           , Expression
           , Expression'(..)
           , FunctionCall(..)
           , FunctionDefinition(..)
           , Identifier
           , IfExpression(..)
           , InlineAssembly(..)
           , LetStatement(..)
           , Module
           , Pattern(..)
           , PatternPos
           , Statement(..)
           , StatementPos
           , StructField(..)
           , StructFieldValue(..)
           , StructFuncCall(..)
           , TupleField(..)
           , Ty(..)
           , Type(..)
           , TypeDefinition(..)
           , TypePos
           , UnaryOperation(..)
           , UnaryOperator(..)
           , WithType(..)
           , untyped
           )
import Escape (escapeAnalysis)
import Environment
    ( Environment
    , Scope(..)
    , addOperand
    , addSymbol
    , addSymbolsPattern
    , enterScope
    , getSymbol
    , leaveScope
    )
import Functions (compilerFunctions, ionDebug)
import GenState (CompilerFunction(..), CompilerFunctions)
import Position (WithPos(..), dummyPos, withPos)
import Region (regionInference)

type Result a = Either Error a

-- |Wrap a transformer result.
(<@>) :: (a -> b) -> AnalyzerState a -> AnalyzerState b
(<@>) wrapper analyzer = do
    result <- analyzer
    return $ wrapper result

-- |Add a type to a null expression.
addTypeToNull :: Expression -> TypePos -> Expression
addTypeToNull expr typ = addTyToNull expr (toTy typ)

-- |Add a type to a null expression.
addTyToNull :: Expression -> Ty -> Expression
addTyToNull withType@WithType { itemNode = WithPos { item = Call _ FunctionCall { callName = WithPos { item = "null" }}}} typ =
    withType { itemType = typ }
addTyToNull value _ = value

-- |Analyze a module.
analyze :: Module -> Bool -> Result (Module, EnumValueTable, Environment, TypeTable, CompilerFunctions)
analyze modu debugMode = do
    stateWithSymbols <- execStateT (mapM collectDef modu) initState
    (modul, AnalyzerRecord { compilerFuncs, enumValueTable, environment, typeTable }) <- runStateT (mapM transDef modu) stateWithSymbols
    let newModule = regionInference $ escapeAnalysis modul
    return (newModule, enumValueTable, environment, typeTable, compilerFuncs)
    where initState = AnalyzerRecord
                    { debugMode
                    , enumValueTable = Map.empty
                    , environment = [Scope
                        { operandTable = Map.empty
                        , symbols = Map.empty
                        }]
                    , compilerFuncs = Map.insert "ionDebug" (ionDebug debugMode) compilerFunctions
                    , methodTable = Map.empty
                    , patternIndex = 0
                    , typeTable = Map.empty
                    }

-- |Get the allowed types for operands in an arithmetic binary operation.
binOpArithmeticOpsType :: Ty -> AnalyzerState Bool
binOpArithmeticOpsType typ = return $ typ `elem` [Ty "Double", Ty "Int"]

-- |Get the allowed types for operands in a compare binary operation.
binOpCompareOpsType :: Ty -> AnalyzerState Bool
binOpCompareOpsType typ = do
    isEnum <- isEnumType typ
    return $ typ `elem` [Ty "Bool", Ty "Char", Ty "Double", Ty "Int"] || isEnum
    where isEnumType typ1 =
              case typ1 of
                   Ty name -> do
                       maybeTypeDefinition <- getType name
                       case maybeTypeDefinition of
                            Just (EnumType _) -> return True
                            _ -> return False
                   _ -> return False

-- |Get the allowed type for operands in a logical operation.
binOpLogicalOpType :: Ty -> AnalyzerState Bool
binOpLogicalOpType typ = return $ typ == Ty "Bool"

-- |Get the allowed types for operands in a modulo binary operation.
binOpModuloOpsType :: Ty -> AnalyzerState Bool
binOpModuloOpsType typ = return $ typ `elem` [Ty "Int"]

-- |Get the type of a binary operation. Check if the type is ok.
binOpType :: BinaryOperator -> Expression -> Expression -> AnalyzerState Ty
binOpType Assign _ _ = return $ TupleTy []
binOpType operator WithType { itemType = typ1, itemNode = op1 }
                   WithType { itemType = typ2, itemNode = op2 } = do
    valid <- valideTy typ1
    if valid then
        if typ1 == typ2 then
            return returnTyp
        else
            newError (position op2) $ messageExpectedType (showTy typ1) (showTy typ2)
    else
        newError (position op1) $ messageWrongTypeBinOp op (showTy typ1)
    where (op, valideTy, returnTyp) = case operator of
            And -> ("&&", binOpLogicalOpType, Ty "Bool")
            Assign -> error "Assign case is managed in a previous pattern matching."
            CustomOper _ -> error "binOpType is not supposed to evaluate a CustomOper."
            Divide -> ("/", binOpArithmeticOpsType, typ1)
            Equal -> ("==", binOpCompareOpsType, Ty "Bool")
            Greater -> (">", binOpCompareOpsType, Ty "Bool")
            GreaterEqual -> (">=", binOpCompareOpsType, Ty "Bool")
            Less -> ("<", binOpCompareOpsType, Ty "Bool")
            LessEqual -> ("<=", binOpCompareOpsType, Ty "Bool")
            Minus -> ("-", binOpArithmeticOpsType, typ1)
            Modulo -> ("%", binOpModuloOpsType, typ1)
            NotEqual -> ("!=", binOpCompareOpsType, Ty "Bool")
            Or -> ("||", binOpLogicalOpType, Ty "Bool")
            Plus -> ("+", binOpArithmeticOpsType, typ1)
            Times -> ("*", binOpArithmeticOpsType, typ1)

-- |Check an assignment operation.
checkAssign :: BinaryOperator -> Expression -> AnalyzerState ()
checkAssign Assign WithType { itemNode = WithPos { item = expr, position }} =
    let showMessage msg =
            let message = errorMessage msg
            in newError position message
    in
    case expr of
         Field TupleField { structFieldMutable = True } -> return ()
         Field TupleField { tupleName, structFieldName, structFieldMutable = False } -> showMessage $ "cannot assign to immutable field `" ++ item tupleName ++ "." ++ item structFieldName ++ "`"
         _ -> showMessage $ "expecting a field at the left side of an assignment operation" ++ show expr
checkAssign _ _ = return ()

-- |Check expected types with actual types in expressions.
checkManyTypes :: [Ty] -> [Expression] -> AnalyzerState ()
checkManyTypes expectedTypes expressions =
    forM_ (zip expectedTypes expressions) $
        \(typ, WithType { itemType, itemNode = WithPos { position }}) ->
            checkTypes typ itemType position $ return ()

-- |Check expected types with actual types in expressions.
checkManyTypesPos :: [TypePos] -> [Expression] -> AnalyzerState ()
checkManyTypesPos expectedTypes = checkManyTypes $ map toTy expectedTypes

-- |Check the expected type with an expression.
-- |If they match, run the analyzer.
checkTypes :: Ty -> Ty -> SourcePos -> AnalyzerState a -> AnalyzerState a
checkTypes expectedType actualType pos analyzer = do
    expected <- expandSynonym expectedType
    actual <- expandSynonym actualType
    if expected == actual then
        analyzer
    else
        let correctType =
                case (expected, actual) of
                     (NullableTy nullableExpected, Ty nonNullableActual) -> nullableExpected == nonNullableActual
                     (AnyType, _) -> True
                     (_, AnyType) -> True
                     _ -> False
        in
        if correctType then
            analyzer
        else
            let message = messageExpectedType (showTy expectedType) (showTy actualType)
            in
            newError pos message

-- |Check the expected type with an expression.
-- |If they match, run the analyzer.
checkTypesPos :: TypePos -> Ty -> SourcePos -> AnalyzerState a -> AnalyzerState a
checkTypesPos expectedType = checkTypes (toTy expectedType)

-- |Collect symbols and types information from a module definition.
collectDef :: Definition -> AnalyzerState ()
collectDef (Function function) =
    collectFunction function
collectDef (MethodDefinition structName method) =
    collectMethod structName method
collectDef (TypeDef WithPos { item = name, position = pos } typeDef) = do
    result <- addType name typeDef
    when (isJust result) $
        newError pos $ messageAlreadyDefined "Type" $ fromJust result
    case typeDef of
         EnumType enumValues -> collectEnumValues enumValues
         _ -> return ()

-- |Collect enum values from an EnumType.
collectEnumValues :: [EnumValue] -> AnalyzerState ()
collectEnumValues =
    iter 0
    where iter index (enumV:enumVs) = do
              let newEnumV = enumV { enumValue = index }
              let ident = enumIdent newEnumV
              result <- addEnumValue (item ident) newEnumV
              when (isJust result) $
                  newError (position ident) $ messageAlreadyDefined "Enum value" $ fromJust result
              iter (index + 1) enumVs
          iter _ _ = return ()

-- |Collect symbols and types from a function.
collectFunction :: FunctionDefinition -> AnalyzerState ()
collectFunction FunctionDefinition { functionName, functionType } = do
    let funcName = item functionName
        name = Name funcName
    resultSymbolFunction <- modifyEnvironment $
        addSymbol funcName $ toTy functionType
    when (isJust resultSymbolFunction) $
        newError (position functionName) $ messageAlreadyDefined "Symbol" funcName
    modifyEnv $ addOperand funcName $ ConstantOperand $ GlobalReference void name

-- |Collect the method symbol.
collectMethod :: Identifier -> FunctionDefinition -> AnalyzerState ()
collectMethod structName@WithPos { item = structIdent, position = identPos } funcDef@FunctionDefinition { functionName, functionType } = do
    resultMethod <- addMethod structIdent funcDef
    when (isJust resultMethod) $
        newError (position functionName) $ messageMethodAlreadyDefined structIdent (item functionName)
    let funcName = structIdent ++ "$" ++ item functionName
        name = Name funcName
        funcType =
            case item functionType of
                 FunctionType types -> FunctionType $ WithPos { item = Type structName, position = identPos } : types
                 _ -> error "Should be a function type"
        funcTypePos = withPos (position functionType) funcType
    resultSymbolMethod <- modifyEnvironment $
        addSymbol funcName $ toTy funcTypePos
    when (isJust resultSymbolMethod) $
        newError (position functionName) $ messageAlreadyDefined "Symbol" funcName
    modifyEnv $ addOperand funcName $ ConstantOperand $ GlobalReference void name

-- |Generate an error message.
errorMessage :: String -> Message
errorMessage message = Message $
    show $ red (text "error:") <+> bold (text message)

-- |Transform a type synonym to its real type.
-- |If it is not a type synonym, return the type as is.
expandSynonym :: Ty -> AnalyzerState Ty
expandSynonym originalType@(Ty ty) = do
    typ <- getType ty
    return $
        case typ of
             Just (TypeSynonym realType) -> toTy realType
             _ -> originalType
expandSynonym ty = return ty

-- |Generate a letSatement from a pattern in function.
genLetFromPattern :: (PatternPos, TypePos) -> AnalyzerState (Maybe StatementPos)
genLetFromPattern (pat@WithPos { item = TuplePattern _, position }, typ) = do
    ident <- getPatternIdent
    return $ Just WithPos
             { item =
                 Let LetStatement
                 { letStatementPattern = pat
                 , letStatementType = typ
                 , letStatementValue = untyped WithPos
                      { item = Const Constant { constantName = WithPos
                                  { item = ident
                                  , position
                                  }
                               }
                      , position
                      }
                 }
             , position
             }
genLetFromPattern _ = return Nothing

-- |Generate a string to indentify a pattern.
getPatternIdent :: AnalyzerState String
getPatternIdent = do
    index <- gets patternIndex
    modify $ \state -> state { patternIndex = index + 1 }
    return $ "pattern$" ++ show index

-- |Get the position of a node with a type and position.
getPos :: WithType (WithPos a) -> SourcePos
getPos WithType { itemNode = WithPos { position }} = position

-- |Get the type of an if expression.
ifType :: [StatementPos] -> Maybe [StatementPos] -> AnalyzerState Ty
ifType trueStatements falseStatements =
    let trueType = statementType $ last trueStatements
        falseType = case falseStatements of
                         Just statements -> statementType $ last statements
                         Nothing -> TupleTy []
    in
    if trueType == falseType then
        return trueType
    else
        let -- The fail is in last true statement when there is no else.
            message =
                case falseStatements of
                    Just _ ->
                        messageExpectedType (showTy trueType) (showTy falseType)
                    Nothing ->
                        messageExpectedType "()" (showTy trueType)
            lastStatement =
                case falseStatements of
                    Just statements -> last statements
                    Nothing -> last trueStatements
        in
        newError (position lastStatement) message

-- |Check if a Ty is a function type.
isFunctionType :: Ty -> Bool
isFunctionType (FuncTy _) = True
isFunctionType _ = False

-- |Make a pattern of type Ident.
makePatternIdent :: String -> SourcePos -> PatternPos
makePatternIdent ident pos = WithPos
    { item =
          Ident WithPos
                { item = ident
                , position = pos
                }
    , position = pos
    }

-- |Generate a message when a symbol is already defined.
messageAlreadyDefined :: String -> String -> Message
messageAlreadyDefined typeSymbol name = errorMessage $
    typeSymbol ++ " `" ++ name ++ "` already defined."

-- |Generate a message for expected type vs actual type.
messageExpectedType :: String -> String -> Message
messageExpectedType expected actual = errorMessage $
    "mismatched types:\n" ++
    " expected `" ++ expected ++ "`\n" ++
    "    found `" ++ actual ++ "`"

-- |Generate a message when a method is already defined.
messageMethodAlreadyDefined :: String -> String -> Message
messageMethodAlreadyDefined structName name = errorMessage $
    "Method `" ++ name ++ "` already defined in struct `" ++ structName ++ "`."

-- |Generate a message when a function is applied with not enough or too many arguments.
messageWrongCountArguments :: Int -> Int -> String -> Message
messageWrongCountArguments expectedCount actualCount functionName = errorMessage $
    "wrong count of arguments for `" ++ functionName ++ "`\n" ++
    " expected " ++ show expectedCount ++ " arguments\n" ++
    "    found " ++ show actualCount ++ " arguments"

-- |Generate a message when the type is not supported in a binary operation.
messageWrongTypeBinOp :: String -> String -> Message
messageWrongTypeBinOp operator typ = errorMessage $
    "wrong type for binary operation `" ++ operator ++ "`\n" ++
    " found `" ++ typ ++ "`"

-- |Generate a message when the type is not supported in an unary operation.
messageWrongTypeUnOp :: String -> String -> Message
messageWrongTypeUnOp operator typ = errorMessage $
    "wrong type for unary operation `" ++ operator ++ "`\n" ++
    " found `" ++ typ ++ "`"

-- |Return a new error.
newError :: SourcePos -> Message -> AnalyzerState a
newError pos message = lift $ Left $ newErrorMessage message pos

-- |Generate an error message about a type not being a structure.
notAStructure :: SourcePos -> String -> AnalyzerState a
notAStructure pos structName =
    let message = errorMessage $ "`" ++ structName ++ "` does not name a structure"
    in
    newError pos message

-- |List of primitives types.
primitivesTypes :: [Ty]
primitivesTypes = [Ty "Bool", Ty "Char", Ty "CharPtr", Ty "Double", Ty "Int"]

-- |Reset the pattern index to 0.
resetPatternIndex :: AnalyzerState ()
resetPatternIndex =
    modify $ \state -> state { patternIndex = 0 }

-- |Get a string representation of a type.
showTy :: Ty -> String
showTy AnyType = "(any type)"
showTy (FuncTy types) = intercalate " > " (map showTy types)
showTy (NullableTy typ) = "?" ++ typ
showTy (TupleTy types) = "(" ++ intercalate ", " (map showTy types) ++ ")"
showTy (Ty typ) = typ
showTy UnTy = error "should not show untyped"

-- |Get the type of a statement.
statementType :: StatementPos -> Ty
statementType WithPos { item = Let _ } = TupleTy []
statementType WithPos { item = Expr WithType { itemType }} = itemType

-- |Get a function return type from a function type.
retType :: Type -> AnalyzerState TypePos
retType (FunctionType types) = return $ last types
retType _ = fail "expecting FuncTyped"

-- |Convert a TypePos to a Ty.
toTy :: TypePos -> Ty
toTy WithPos { item = FunctionType types } = FuncTy $ map toTy types
toTy WithPos { item = NullableType WithPos { item = typ }} = NullableTy typ
toTy WithPos { item = TupleType types } = TupleTy $ map toTy types
toTy WithPos { item = Type WithPos { item = typ }} = Ty typ

-- |Analyze a definition.
transDef :: Definition -> AnalyzerState Definition
transDef (Function function) = Function <@> transFunction function
transDef (MethodDefinition structName function) =
    let method = functionName function
        methodName = item method
        struct = item structName
        structPos = position structName
        funcName = WithPos { item = struct ++ "$" ++ methodName, position = position method }
        FunctionDefinition { functionParameters, functionStatements, functionType } = function
        funcType =
            case item functionType of
                 FunctionType types -> WithPos {
                     item = FunctionType $ WithPos { item = Type structName, position = structPos } : types,
                     position = structPos
                 }
                 _ -> error "Not a function type"
    in
    Function <@> transFunction
        FunctionDefinition { callEscapingFunctions = False
                           , escapingParameters = []
                           , functionHasEscapes = False
                           , functionEscapes = []
                           , functionName = funcName
                           , functionParameters
                           , functionStatements
                           , functionType = funcType
                           }
transDef (TypeDef name typeDef) = TypeDef name <@> transTypeDef typeDef

-- |Analyze an expression.
transExpr :: Expression -> AnalyzerState Expression
transExpr expr = do
    let expression = itemNode expr
    (itemTy, node) <-
            case item expression of
                 BinOp (BinaryOperation op op1 op2) ->
                     case op of
                          And -> do
                              tyOp1 <- transExpr op1
                              tyOp2 <- transExpr op2
                              binTy <- binOpType op tyOp1 tyOp2
                              checkAssign op tyOp1
                              let trueStatement = Expr WithType
                                      { itemNode = dummyPos $ EnumExpression $ dummyPos "True"
                                      , itemType = binTy
                                      }
                                  elseStatement = Expr WithType
                                      { itemNode = dummyPos $ EnumExpression $ dummyPos "False"
                                      , itemType = binTy
                                      }
                                  secondIfExpression = If IfExpression
                                      { condition = tyOp2
                                      , trueStatements = [dummyPos trueStatement]
                                      , elseStatements = Just [dummyPos elseStatement]
                                      }
                                  firstIfTrueStatement = Expr WithType
                                      { itemNode = dummyPos secondIfExpression
                                      , itemType = binTy
                                      }
                              return (binTy, If IfExpression
                                  { condition = tyOp1
                                  , trueStatements = [dummyPos firstIfTrueStatement]
                                  , elseStatements = Just [dummyPos elseStatement]
                                  })
                          CustomOper callName ->
                              transFunctionCall FunctionCall
                              { callArguments = [op1, op2]
                              , callName
                              }
                          Or -> do
                              tyOp1 <- transExpr op1
                              tyOp2 <- transExpr op2
                              binTy <- binOpType op tyOp1 tyOp2
                              checkAssign op tyOp1
                              let trueStatement = Expr WithType
                                      { itemNode = dummyPos $ EnumExpression $ dummyPos "True"
                                      , itemType = binTy
                                      }
                                  elseStatement = Expr WithType
                                      { itemNode = dummyPos $ EnumExpression $ dummyPos "False"
                                      , itemType = binTy
                                      }
                                  secondIfExpression = If IfExpression
                                      { condition = tyOp2
                                      , trueStatements = [dummyPos trueStatement]
                                      , elseStatements = Just [dummyPos elseStatement]
                                      }
                                  firstIfFalseStatement = Expr WithType
                                      { itemNode = dummyPos secondIfExpression
                                      , itemType = binTy
                                      }
                              return (binTy, If IfExpression
                                  { condition = tyOp1
                                  , trueStatements = [dummyPos trueStatement]
                                  , elseStatements = Just [dummyPos firstIfFalseStatement]
                                  })
                          _ -> do
                              tyOp1 <- transExpr op1
                              tyOp2 <- transExpr op2
                              binTy <- binOpType op tyOp1 tyOp2
                              checkAssign op tyOp1
                              return (binTy, BinOp $ BinaryOperation op tyOp1 tyOp2)
                 Call _ functionCall -> transFunctionCall functionCall
                 char@(CharLiteral _) -> return (Ty "Char", char)
                 Const constant@Constant { constantName } -> do
                     env <- getEnv
                     let typ = getSymbol env (item constantName)
                     case typ of
                          Just ty -> return (ty, Const constant)
                          Nothing -> unknownName (position expression) (item constantName)
                 double@(DoubleLiteral _) -> return (Ty "Double", double)
                 enum@(EnumExpression WithPos { item = name, position = pos }) -> do
                     result <- getEnumValue name
                     case result of
                          Just enumV -> return (Ty (item $ enumTy enumV), enum)
                          Nothing -> unknownEnumValue pos name
                 string@(GlobalString _) -> return (Ty "CharPtr", string)
                 If IfExpression { condition, trueStatements, elseStatements } -> do
                     -- Enter Scope
                     modifyEnv enterScope
                     newTrueStatements <- mapM transStatement trueStatements
                     -- Leave Scope
                     modifyEnv leaveScope
                     -- Enter Scope
                     modifyEnv enterScope
                     newElseStatements <-
                         case elseStatements of
                              Just statements -> Just <$> mapM transStatement statements
                              Nothing -> return Nothing
                     -- Leave Scope
                     modifyEnv leaveScope
                     newCondition <- transExpr condition
                     checkTypes (Ty "Bool") (itemType newCondition) (position $ itemNode newCondition) $ do
                         ifTy <- ifType newTrueStatements newElseStatements
                         return (ifTy, If IfExpression
                             { condition = newCondition
                             , trueStatements = newTrueStatements
                             , elseStatements = newElseStatements
                             })
                 InlineAsm InlineAssembly { assembly, inputArguments, inputConstraints, outputConstraints } -> do
                     inputArgs <- mapM transExpr inputArguments
                     return (Ty "Int", InlineAsm InlineAssembly
                         { assembly
                         , inputArguments = inputArgs
                         , inputConstraints
                         , outputConstraints
                         })
                 int@(IntegerLiteral _) -> return (Ty "Int", int)
                 MethodCall methodCall -> transMethodCall methodCall
                 StringLiteral stringLiteral -> do
                     let createExpr item = expr { itemNode = expression { item }}
                         stringExpr = createExpr $ GlobalString stringLiteral
                         lengthExpr = createExpr $ IntegerLiteral $ fromIntegral $ length stringLiteral
                         fields =
                             [ StructFieldValue { fieldValueName = dummyPos "chars", fieldValue = stringExpr }
                             , StructFieldValue { fieldValueName = dummyPos "length", fieldValue = lengthExpr }
                             ]
                     transStructExpr (dummyPos "String") fields
                 StructExpression name fields -> transStructExpr name fields
                 Tuple structType escape exprs -> do
                     typedExprs <- mapM transExpr exprs
                     let types = map itemType typedExprs
                     return (TupleTy types, Tuple structType escape typedExprs)
                 tupleField@(Field TupleField { tupleName, tupleIndex }) -> do
                     env <- getEnv
                     let typeIdent = getSymbol env (item tupleName)
                     case typeIdent of
                          Just (TupleTy types) -> return (types !! fromIntegral tupleIndex, tupleField)
                          Just ty -> wrongTypeTupleIndex (position expression) ty tupleIndex
                          Nothing -> unknownName (position expression) $ item tupleName
                 UnOp (UnaryOperation op op1) ->
                     case op of
                          UnaryCustom callName ->
                              transFunctionCall FunctionCall
                              { callArguments = [op1]
                              , callName
                              }
                          _ -> do
                             tyOp1 <- transExpr op1
                             unTy <- unOpType op tyOp1
                             return (unTy, UnOp $ UnaryOperation op tyOp1)
    let record = itemNode expr
        newExpression = record { item = node }
    return expr { itemNode = newExpression, itemType = itemTy }

-- |Analyze a function.
transFunction :: FunctionDefinition -> AnalyzerState FunctionDefinition
transFunction FunctionDefinition { functionName, functionParameters, functionStatements, functionType } = do
    let types =
            case item functionType of
                 FunctionType typs -> typs
                 _ -> fail "expecting function type"
    mapM_ (\typePos -> validateType (position typePos) (toTy typePos)) types
    -- Enter Scope.
    modifyEnv enterScope
    newFunctionParameters <-
        mapM transParam (zip functionParameters types)
    resetPatternIndex
    letMaybeStatements <-
        mapM genLetFromPattern (zip functionParameters types)
    resetPatternIndex
    let letStatements = catMaybes letMaybeStatements
    statements <- mapM transStatement (letStatements ++ itemNode functionStatements)
    -- Leave Scope.
    modifyEnv leaveScope
    let lastStatement = last statements
    let blockType = statementType lastStatement
        funcStatements = WithType { itemType = blockType, itemNode = statements }
    returnType <- retType $ item functionType
    checkTypesPos returnType blockType (position lastStatement) $
        return FunctionDefinition
            { callEscapingFunctions = False
            , escapingParameters = []
            , functionEscapes = []
            , functionHasEscapes = False
            , functionName
            , functionParameters = newFunctionParameters
            , functionStatements = funcStatements
            , functionType
            }

-- |Analyze a function call.
transFunctionCall :: FunctionCall -> AnalyzerState (Ty, Expression')
transFunctionCall call@FunctionCall { callArguments, callName } =  do
    let funcName = item callName
    newCallArguments <- mapM transExpr callArguments
    env <- getEnv
    let funcType = getSymbol env (item callName)
    case funcType of
         Just typ ->
             let (argumentsType, returnType) =
                     case typ of
                          (FuncTy types) -> (init types, last types)
                          _ -> ([], typ)
                 newArgs = zipWith addTyToNull newCallArguments argumentsType
             in
             if isFunctionType typ || funcName == "main" then do
                when (length argumentsType /= length newCallArguments) $
                    newError (position callName) $
                        messageWrongCountArguments (length argumentsType) (length newCallArguments) funcName
                checkManyTypes argumentsType newArgs
                let newCall = call { callArguments = newArgs }
                return (returnType, Call NoEscape newCall)
             else
                return (returnType, Const Constant
                       { constantName = callName
                       })
         Nothing -> do
             funcs <- gets compilerFuncs
             case Map.lookup funcName funcs of
                  Just func -> do
                      let compilerFuncType = compilerFunctionType func
                          paramTypes =
                              if funcName == "isNull" then
                                  case item $ itemNode $ head newCallArguments of
                                       Call _ FunctionCall { callName = WithPos { item = "null" }} ->
                                           [NullableTy "String"] -- NOTE: because isNull null does not require any specific type, send any type.
                                       _ -> init compilerFuncType
                              else
                                  init compilerFuncType
                          returnType = last compilerFuncType
                          newArgs = zipWith addTyToNull newCallArguments paramTypes
                          newCall = call { callArguments = newArgs }
                      checkManyTypes paramTypes newCallArguments
                      return (returnType, Call NoEscape newCall)
                  Nothing -> unknownName (position callName) (item callName)

-- |Analyze a let statement.
transLet :: LetStatement -> AnalyzerState LetStatement
transLet letStatement@LetStatement { letStatementPattern, letStatementType, letStatementValue } = do
    typedStatementValue <- transExpr letStatementValue
    let letTypedStatementValue = addTypeToNull typedStatementValue letStatementType
    validateType (position letStatementType) (toTy letStatementType)
    let exprType = itemType letTypedStatementValue
    checkTypesPos letStatementType exprType (getPos letTypedStatementValue) $ do
        result <- modifyEnvironment $
            addSymbolsPattern letStatementPattern (toTy letStatementType)
        when (isJust result) $
            newError (position letStatementPattern) $
                messageAlreadyDefined "Symbol" (fromJust result)
        return letStatement { letStatementValue = letTypedStatementValue }

-- |Analyze a method call.
transMethodCall :: StructFuncCall -> AnalyzerState (Ty, Expression')
transMethodCall StructFuncCall { methodCallArguments, methodCallName, structName } =  do
    let methodName = item methodCallName
    env <- getEnv
    let ident = item structName
        structType = getSymbol env ident
    (typ, structIdent) <- case structType of
                Just (Ty ty) -> do
                    result <- getType ty
                    return (result, ty)
                Just (NullableTy typ) ->
                    return (Nothing, typ)
                Nothing -> fail $ "struct name `" ++ ident ++ "` not found"
                _ -> fail "cannot call method on this type"
    let isNullableFieldAccess = isNothing typ && methodName == "val"
    if isNullableFieldAccess then
        let nullableType = structIdent in
        return (Ty nullableType, Const Constant
               { constantName = structName
               })
    else do
        let structFields =
                case typ of
                     Just (StructDefinition fields) ->
                         let toTuple (index, field@StructField { fieldName = WithPos { item = name } }) = (name, (field, index))
                         in zipWith (curry toTuple) [0..] fields
                     _ -> fail "struct type does not exist"
        case lookup methodName structFields of
             Just (StructField { fieldMutable, fieldName, fieldType }, fieldIndex) ->
                 return (toTy fieldType, Field TupleField
                                        { tupleName = structName
                                        , tupleIndex = fieldIndex
                                        , structFieldName = fieldName
                                        , structFieldMutable = fieldMutable
                                        , structFieldType = toTy fieldType
                                        })
             Nothing -> do
                 method <- getMethod structIdent methodName
                 structMethod <-
                         case method of
                              Just function -> return function
                              _ -> unknownMethod (position methodCallName) structIdent methodName
                 let callName = WithPos
                                { item = structIdent ++ "$" ++ item (functionName structMethod)
                                , position = position methodCallName
                                }
                     structVar = untyped $ dummyPos $ Call NoEscape FunctionCall { callArguments = [], callName = structName }
                 transFunctionCall FunctionCall
                                   { callArguments = structVar : methodCallArguments
                                   , callName
                                   }

-- |Analyze a function parameter.
transParam :: (PatternPos, TypePos) -> AnalyzerState PatternPos
transParam (WithPos { item = Hole, position }, _) = do
    ident <- getPatternIdent
    return $ makePatternIdent ident position
transParam (param@WithPos { item = Ident WithPos { item = ident } }, typ) =  do
    result <- modifyEnvironment $ addSymbol ident (toTy typ)
    when (isJust result) $
        newError (position param) $ messageAlreadyDefined "Parameter" ident
    return param
transParam (WithPos { item = TuplePattern _, position }, typ) = do
    ident <- getPatternIdent
    let patternIdent = WithPos
                           { item = ident
                           , position = position
                           }
    result <- modifyEnvironment $ addSymbol (item patternIdent) (toTy typ)
    when (isJust result) $
       fail $ "A parameter name generated to do pattern matching in parameter is supposed to be unique.\n" ++
            " generated name : `" ++ fromJust result ++ "`"
    return WithPos
        { item = Ident patternIdent
        , position = position
        }

-- |Analyze a statement
transStatement :: StatementPos -> AnalyzerState StatementPos
transStatement statement@WithPos { item = Expr expr } = do
    expression <- transExpr expr
    return statement { item = Expr expression }
transStatement statement@WithPos { item = Let letStatement } = do
    newLetStatement <- transLet letStatement
    return statement { item = Let newLetStatement }

-- |Analyze a structure expression.
transStructExpr :: Identifier -> [StructFieldValue] -> AnalyzerState (Ty, Expression')
transStructExpr WithPos { item = ident, position = identPos } fields = do
    structType <- getType ident
    case structType of
         Just (StructDefinition fieldTypes) -> do
            let types = map (\StructField { fieldName, fieldType } -> (item fieldName, fieldType)) fieldTypes
                fieldPositions = map (\StructFieldValue { fieldValueName } ->
                   (item fieldValueName, position fieldValueName)) fields
                exprs = map (\StructFieldValue { fieldValueName, fieldValue } ->
                   (item fieldValueName, fieldValue)) fields
                values = map exprToValue types
                expectedTypes = map snd types
                exprToValue (name, typ) = (lookup name exprs, typ)
                expectedFields = Set.fromList $ map fst types
                actualFields = Set.fromList $ map fst exprs
            if all isJust $ map fst values then
               if length values < length exprs then
                   let unknownFields = Set.difference actualFields expectedFields
                       unknownFieldWithPos = head $ map addFieldPos $ Set.toList unknownFields
                       addFieldPos f = (f, fromJust $ lookup f fieldPositions)
                       (field, fieldPos) = unknownFieldWithPos
                   in
                   unknownField fieldPos ident field
               else do
                   fieldValues <- mapM (\(v, typ) -> do
                        newValue <- transExpr $ fromJust v
                        return $ addTypeToNull newValue typ) values
                   checkManyTypesPos expectedTypes fieldValues
                   return (Ty ident, Tuple (Just ident) NoEscape fieldValues)
            else
               let missingFields = Set.difference expectedFields actualFields
               in wrongFields identPos $ Set.toList missingFields
         _ -> notAStructure identPos ident

-- |Analyze a type definition.
transTypeDef :: TypeDefinition -> AnalyzerState TypeDefinition
transTypeDef (StructDefinition fields) =
    mapM_ (\StructField {fieldType} -> validateType (position fieldType) (toTy fieldType)) fields >>
    let newFields = zipWith (curry assignFieldIndex) [0..] fields
    in return $ StructDefinition newFields
    where assignFieldIndex (fieldIndex, structField) = structField { fieldIndex }
transTypeDef synonym@(TypeSynonym typ@WithPos{position}) =
    validateType position (toTy typ) >> return synonym
transTypeDef typeDef = return typeDef

-- |Get the type of a unary operation.
unOpType :: UnaryOperator -> Expression -> AnalyzerState Ty
unOpType op WithType { itemType = typ, itemNode = operand }
    | typ == Ty "Int" || typ == Ty "Double" = return typ
    | otherwise = newError (position operand) $ messageWrongTypeUnOp operator $ showTy typ
        where operator = case op of
                             UnaryCustom _ -> error "unOpType is not supposed to evaluate a UnaryCustom."
                             UnaryMinus -> "-"
                             UnaryPlus -> "+"

-- |Add an error about un unknown enum value.
unknownEnumValue :: SourcePos -> String -> AnalyzerState a
unknownEnumValue pos name = newError pos $ errorMessage $ "unresolved enum value `" ++ name ++ "`"

-- |Show an error about an unknown structure field.
unknownField :: SourcePos -> String -> String -> AnalyzerState a
unknownField pos structName field =
    let message = errorMessage $ "structure `" ++ structName ++ "` has no field named `" ++ field ++ "`"
    in
    newError pos message

-- |Add an error about un unknown name.
unknownName :: SourcePos -> String -> AnalyzerState a
unknownName pos name = newError pos $ errorMessage $ "unresolved name `" ++ name ++ "`"

nullableNotValid :: SourcePos -> String -> AnalyzerState a
nullableNotValid pos name = newError pos $ errorMessage $ "Type `" ++ name ++ "` cannot be nullable."

-- |Add an error about an unknown method.
unknownMethod :: SourcePos -> String -> String -> AnalyzerState a
unknownMethod pos structName methodName =
    let message = errorMessage $ "no method named `" ++ methodName ++ "` found for type `" ++ structName ++ "`"
    in
    newError pos message

-- |Validate a type.
validateType :: SourcePos -> Ty -> AnalyzerState ()
validateType _ AnyType = return ()
validateType pos (FuncTy typs) = mapM_ (validateType pos) typs
validateType pos (NullableTy name) = do
    expandedType <- expandSynonym (Ty name)
    if expandedType `elem` primitivesTypes then
        nullableNotValid pos name
    else
        validateType pos (Ty name)
validateType pos (TupleTy typs) = mapM_ (validateType pos) typs
validateType pos typ@(Ty name) = do
    maybeType <- getType name
    unless (isJust maybeType || typ `elem` primitivesTypes) $
        typeNotValid pos name
validateType _ UnTy = return ()

-- |Type doesn't exists.
typeNotValid :: SourcePos -> String -> AnalyzerState a
typeNotValid pos name = newError pos $ errorMessage $ "Type `" ++ name ++ "` doesn't exists."

-- |Show an error about wrong structure fields.
wrongFields :: SourcePos -> [String] -> AnalyzerState a
wrongFields pos missingFields =
    let message = errorMessage $ "missing fields: `" ++ intercalate "`, `" missingFields ++ "`"
    in
    newError pos message

-- |Add an error about using tuple indexing on a type other than a tuple.
wrongTypeTupleIndex :: SourcePos -> Ty -> Integer -> AnalyzerState a
wrongTypeTupleIndex pos typ index =
    let message = errorMessage $ "attempted tuple index `" ++ show index ++ "` on type `" ++ showTy typ ++ "`, but the type was not a tuple"
    in newError pos message
