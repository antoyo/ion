{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Parser
Description: The ion parser.
-}

module Parser (parse) where

import Control.Monad.State (State, get, put)
import Data.Maybe (fromMaybe, isJust)
import Data.List (intercalate)

import Text.Parsec ((<|>), (<?>), char, eof, getPosition, lower, many, many1, noneOf, optionMaybe, runParserT, sepBy1)
import Text.Parsec.Error (ParseError)
import Text.Parsec.Expr (Assoc(AssocLeft, AssocNone), Operator(Infix, Prefix), buildExpressionParser)
import Text.Parsec.Indent (IndentParser, block, checkIndent, indented, runIndent, same, sameOrIndented, withBlock)
import Text.Parsec.Pos (SourcePos)

import Ast ( BinaryOperation(..)
           , BinaryOperator(..)
           , Definition(..)
           , EnumValue(..)
           , ExprEscape(..)
           , Expression'(..)
           , Expression
           , FunctionCall(..)
           , FunctionDefinition(..)
           , Identifier
           , IfExpression(..)
           , InlineAssembly(..)
           , LetStatement(..)
           , Module
           , Pattern(..)
           , PatternPos
           , Statement(..)
           , StatementPos
           , StructField(..)
           , StructFieldValue(..)
           , StructFuncCall(..)
           , TupleField(..)
           , Ty(..)
           , Type(..)
           , TypeDefinition(..)
           , TypePos
           , UnaryOperation(..)
           , UnaryOperator(..)
           , WithType
           , untyped
           , withPosUntyped
           )
import Lexer (ParserPos, braces, charLiteral, colon, commaSep, commaSep1, customOperatorIdDef, decimal, hole, identifier, majIdentifier, operator, parens, reserved, reservedOp, stringLiteral, naturalOrFloat, whiteSpace, wrapInPos, wrapInPosUntyped)
import Position (WithPos(..), dummyPos)

type ParserIndent a = IndentParser String () a

-- |Wrap a parser result with its position, untyped and another wrapper.
(<@>) :: (a -> b) -> ParserIndent a -> ParserIndent (WithType (WithPos b))
(<@>) wrapper parser = do
    result <- wrapInPos parser
    return $ untyped $ wrapper <$> result

-- |Wrap a parser result with its position and another wrapper.
(<^>) :: (a -> b) -> ParserIndent a -> ParserIndent (WithPos b)
(<^>) wrapper parser = do
    result <- wrapInPos parser
    return $ wrapper <$> result

-- |Parse a top-level definition.
definition :: ParserIndent [Definition]
definition =
    (:[]) . Function <$> function <|>
    methods <|>
    (:[]) <$> typeDef <*
    whiteSpace

-- |Parse an expression.
expression :: ParserIndent Expression
expression = buildExpressionParser operatorTable factor

-- |The base operator table.
operatorTable :: [[Operator String () (State SourcePos) Expression]]
operatorTable =
    [ [unop]
    , [ binary "*" (BinaryOperation Times) AssocLeft
      , binary "/" (BinaryOperation Divide) AssocLeft
      , binary "%" (BinaryOperation Modulo) AssocLeft
      ]
    , [ binary "+" (BinaryOperation Plus) AssocLeft
      , binary "-" (BinaryOperation Minus) AssocLeft
      ]
    , [ binary ">=" (BinaryOperation GreaterEqual) AssocNone
      , binary ">" (BinaryOperation Greater) AssocNone
      , binary "<=" (BinaryOperation LessEqual) AssocNone
      , binary "<" (BinaryOperation Less) AssocNone
      , binary "==" (BinaryOperation Equal) AssocNone
      , binary "!=" (BinaryOperation NotEqual) AssocNone
      ]
    , [ binary "&&" (BinaryOperation And) AssocLeft
      ]
    , [ binary "||" (BinaryOperation Or) AssocLeft
      ]
    , [ binary "=" (BinaryOperation Assign) AssocNone
      ]
    , [binop]
    ]

-- |Parse a function call argument.
argument :: ParserIndent Expression
argument = literal
       <|> constantOrMethodCall
       <|> parensOrTuple
       <|> enumOrStructExpr

-- |Parse binary expression.
binary :: String
          -> (Expression -> Expression -> BinaryOperation)
          -> Assoc
          -> Operator String () (State SourcePos) Expression
binary op wrapper assoc = do
    let operation = do
            sameOrIndented
            position <- getPosition
            reservedOp op
            return $ \result1 result2 ->
                withPosUntyped position $ BinOp $ wrapper result1 result2
    Infix operation assoc

-- |Custom binary operator.
binop :: Operator String () (State SourcePos) Expression
binop =
    let operation = do
            sameOrIndented
            position <- getPosition
            oper <- operator
            return $ \result1 result2 ->
                withPosUntyped position $ BinOp $ BinaryOperation (CustomOper oper) result1 result2
    in
    Infix operation AssocLeft

-- |Built-in or custom unary expression.
unop :: Operator String () (State SourcePos) Expression
unop =
    let operation = do
            position <- getPosition
            oper <- unaryOperator
            return $ \result ->
                withPosUntyped position $ UnOp $ UnaryOperation oper result
    in
    Prefix operation

-- |Parse a unary operator.
unaryOperator :: ParserIndent UnaryOperator
unaryOperator = do
    op <- operator
    return $
        case item op of
             "-" -> UnaryMinus
             "+" -> UnaryPlus
             _ -> UnaryCustom op

-- |Parse a constant or a method call.
constantOrMethodCall :: ParserIndent Expression
constantOrMethodCall = do
    position <- getPosition
    callName <- identifier
    result <- optionMaybe (reservedOp ".")
    call <- case result of
             Just _ -> methodCallWithoutArgs callName
             Nothing -> return $ Call NoEscape FunctionCall
                       { callArguments = []
                       , callName
                       }
    return $ withPosUntyped position call

-- |Parse an else with its statements.
elseExpression :: ParserIndent [StatementPos]
elseExpression = do
    checkIndent
    pos <- getPosition
    reserved "else"
    put pos
    elseIf <|>
        block statement

-- |Parse an if with its statements.
-- |To be used for an "else if".
elseIf :: ParserIndent [StatementPos]
elseIf = do
    same
    condition <- ifCondition
    indented
    trueStatements <- block statement
    elseStatements <- optionMaybe elseExpression
    expr <- If <@>
        return IfExpression
        { condition
        , trueStatements
        , elseStatements
        }
    statements <- wrapInPos $ return $ Expr expr
    return [statements]

-- |Parse an enum expression or a struct expression.
enumOrStructExpr :: ParserIndent Expression
enumOrStructExpr = wrapInPosUntyped $ do
    name <- majIdentifier
    fields <- optionMaybe $ braces $ commaSep1 structFieldValue
    case fields of
         Just structFields -> return $ StructExpression name structFields
         Nothing -> return $ EnumExpression name

-- |Parse an enumeration.
enumType :: ParserIndent Definition
enumType = do
    reserved "enum"
    name <- majIdentifier
    enums <- block majIdentifier
    return $ TypeDef name $ EnumType $
        map (\ident -> EnumValue { enumIdent = ident, enumValue = 0, enumTy= name }) enums

-- |Parse a factor.
factor :: ParserIndent Expression
factor = literal
     <|> functionOrMethodCallOrInlineAsm
     <|> If <@> ifExpression
     <|> parensOrTuple
     <|> enumOrStructExpr
     <?> "expression"

-- |Parse a function definition.
function :: ParserIndent FunctionDefinition
function =
    withBlock combine functionHeader statement
    <?> "function definition"
    where combine header functionStatements =
            header { functionStatements = untyped functionStatements }

-- |Parse a function call, a method call or an asm expression.
functionOrMethodCallOrInlineAsm :: ParserIndent Expression
functionOrMethodCallOrInlineAsm = do
    position <- getPosition
    callName <- identifier
    call <-
        if item callName == "asm" then do
            assembly <- inlineAsm
            return $ InlineAsm assembly
        else
            methodCall callName <|>
            do
                arguments <- functionCallArguments callName
                return $ Call NoEscape arguments
    return $ withPosUntyped position call

-- |Parse function call arguments.
functionCallArguments :: Identifier -> ParserIndent FunctionCall
functionCallArguments callName = do
    callArguments <- saveStartIndent $ many $ indented >> argument
    return FunctionCall
        { callArguments
        , callName
        }

-- |Parse a function header (name and type).
functionHeader :: ParserIndent FunctionDefinition
functionHeader = do
    _ <- colon
    functionType <- funcType
    checkIndent -- Second line has the same indent than the first line.
    functionName <- customOperatorIdDef
                <|> identifier
    functionParameters <- many parsePattern
    reservedOp "="
    return FunctionDefinition
           { callEscapingFunctions = False
           , escapingParameters = []
           , functionEscapes = []
           , functionHasEscapes = False
           , functionName
           , functionParameters
           , functionStatements = untyped []
           , functionType
           }

-- |Parse a function type.
funcType :: ParserIndent TypePos
funcType = FunctionType <^> (typ `sepBy1` reservedOp ">")

-- |Parse an if condition.
ifCondition :: ParserIndent Expression
ifCondition = saveStartIndent $
    reserved "if" *>
    expression <*
    reserved "then"

-- |Parse an if expression.
ifExpression :: ParserIndent IfExpression
ifExpression = do
    ifExpr <- withBlock combine ifCondition statement
    elseStatements <- optionMaybe elseExpression
    return ifExpr { elseStatements }
    where combine condition trueStatements =
              IfExpression
              { condition
              , trueStatements
              , elseStatements = Nothing
              }

-- |Parse an inline assembly block.
inlineAsm :: ParserIndent InlineAssembly
inlineAsm = do
    reservedOp "("
    assembly <- many inlineAsmToken
    outputConstraints <- inlineAsmOutputs
    (inputArguments, inputConstraints) <- inlineAsmInputs
    reservedOp ")"
    return InlineAssembly
           { assembly = concat assembly
           , inputArguments
           , inputConstraints
           , outputConstraints
           }

-- |Parse inline assembly input constraints.
inlineAsmInputs :: ParserIndent ([Expression], String)
inlineAsmInputs = do
    result <- optionMaybe $ do
        reservedOp "|"
        commaSep $ do
            constraint <- lower
            variable <- parens expression
            whiteSpace
            return (variable, constraint)
    return $
        case result of
             Just res ->
                let constraints = map ((: []) . snd) res
                    expressions = map fst res
                in
                (expressions, intercalate ", " constraints)
             Nothing -> ([], "")

-- |Parse inline assembly output constraints.
inlineAsmOutputs :: ParserIndent String
inlineAsmOutputs = do
    result <- optionMaybe $ do
        reservedOp "|"
        reservedOp "="
        rest <- lower
        whiteSpace
        return $ '=' : [rest]
    return $ fromMaybe "" result

-- |Parse an assembly instruction.
inlineAsmToken :: ParserIndent String
inlineAsmToken = do
    token <- many1 $ noneOf ")| "
    whiteSpace
    return $ token ++ " "

-- |Parse a let statement.
letStatement :: ParserIndent LetStatement
letStatement = saveStartIndent $ do
    reserved "let"
    indented
    letStatementPattern <- parsePattern
    indented
    letStatementType <- typ
    indented
    reservedOp "="
    indented
    letStatementValue <- expression
    return LetStatement
           { letStatementPattern
           , letStatementType
           , letStatementValue
           }

-- |Parse a literal expression.
literal :: ParserIndent Expression
literal = CharLiteral <@> charLiteral
     <|> StringLiteral <@> stringLiteral
     <|> naturalOrFloatWrapper <@> naturalOrFloat

-- |Parse method definitions.
methods :: ParserIndent [Definition]
methods = do
    (structName, structMethods) <- withBlock (,) (reserved "impl" >> majIdentifier) function
    let methodDefinitions = map (MethodDefinition structName) structMethods
    return methodDefinitions
    <?> "impl block"

-- |Parse a method call or a field tuple access.
methodCall :: Identifier -> ParserIndent Expression'
methodCall structName =
    reservedOp "." >>
    (do
        ident <- identifier
        call <- functionCallArguments ident
        return $ MethodCall StructFuncCall
               { methodCallArguments = callArguments call
               , methodCallName = callName call
               , structName
               }
    <|>
    tupleFieldAccess structName)

-- |Parse a method call without arguments or a field tuple access.
methodCallWithoutArgs :: Identifier -> ParserIndent Expression'
methodCallWithoutArgs structName =
    do
        ident <- identifier
        return $ MethodCall StructFuncCall
               { methodCallArguments = []
               , methodCallName = ident
               , structName
               }
    <|>
    tupleFieldAccess structName

-- |Parse a module (file).
modul :: ParserIndent Module
modul = do
    whiteSpace
    definitions <- block definition
    eof
    return $ concat definitions

-- |Apply either the IntegerLiteral wrapper or the DoubleLiteral wrapper.
naturalOrFloatWrapper :: Either Integer Double -> Expression'
naturalOrFloatWrapper = either IntegerLiteral DoubleLiteral

-- |Parse an nullable Type.
nullableType :: ParserIndent Identifier
nullableType = char '?' *> majIdentifier

-- |Parse either a tuple or a parenthesised expression.
parensOrTuple :: ParserIndent Expression
parensOrTuple = do
    position <- getPosition
    result <- parens (commaSep expression)
    let newResult = case result of
         [expr] -> expr
         tuple -> withPosUntyped position $ Tuple Nothing NoEscape tuple
    return newResult

-- |Parse a file.
parse :: FilePath -> IO (Either ParseError Module)
parse = parseFromFile modul

-- |Parse a file with a indentation-aware parser.
parseFromFile :: ParserIndent a -> String -> IO (Either ParseError a)
parseFromFile parser filename = do
    stdlib <- readFile "stdlib/lib.ion"
    input <- readFile filename
    let content = input ++ "\n" ++ stdlib
    return $ runIndent filename $ runParserT parser () filename content

-- |Parse a pattern.
parsePattern :: ParserPos Pattern
parsePattern = wrapInPos $
        Ident <$> identifier
    <|> TuplePattern <$> tuplePattern
    <|> do
        hole
        return Hole

-- |Parse a statement.
statement :: ParserIndent StatementPos
statement = wrapInPos $
        Expr <$> expression
    <|> Let <$> letStatement

-- |Save the start indent, parse with the parser and restore the start indent.
-- |This is useful when used in conjunction with indentation parser (indented, withBlock, …).
saveStartIndent :: ParserIndent a -> ParserIndent a
saveStartIndent parser = do
    s <- get
    result <- parser
    put s
    return result

-- |Parse a struct field definition.
structField :: ParserIndent StructField
structField = do
    indented
    mutable <- optionMaybe $ reserved "mut"
    fieldName <- identifier
    fieldType <- typ
    return StructField { fieldIndex = 0, fieldMutable = isJust mutable, fieldName, fieldType }

-- |Parse a struct field name and value.
structFieldValue :: ParserIndent StructFieldValue
structFieldValue = do
    fieldValueName <- identifier
    reservedOp "="
    fieldValue <- expression
    return StructFieldValue { fieldValue, fieldValueName }

-- |Parse a struct type definition.
structType :: ParserIndent Definition
structType = do
    reserved "struct"
    name <- majIdentifier
    fields <- many1 structField
    return $ TypeDef name $ StructDefinition fields

-- |Parse a tuple field access.
tupleFieldAccess :: Identifier -> ParserIndent Expression'
tupleFieldAccess tupleName = do
    tupleIndex <- decimal
    return $ Field TupleField
                    { tupleName
                    , tupleIndex
                    , structFieldName = dummyPos ""
                    , structFieldMutable = False
                    , structFieldType = UnTy
                    }

-- |Parse a tuple pattern.
tuplePattern :: ParserIndent [PatternPos]
tuplePattern = parens (commaSep parsePattern)

-- |Parse a tuple type.
tupleType :: ParserIndent [TypePos]
tupleType = parens (commaSep typ)

-- |Parse a type.
typ :: ParserPos Type
typ = NullableType <^> nullableType
  <|> TupleType <^> tupleType
  <|> Type <^> majIdentifier
  <?> "type"

-- |Parse a type definition.
typeDef :: ParserIndent Definition
typeDef = enumType
      <|> typeSynonym
      <|> structType
      <?> "type definition"

-- |Parse a type synonym definition.
typeSynonym :: ParserIndent Definition
typeSynonym = do
    reserved "type"
    name <- majIdentifier
    reservedOp "="
    result <- typ
    return $ TypeDef name $ TypeSynonym result
