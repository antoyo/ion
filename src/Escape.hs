{-
 - Copyright (C) 2016  Boucher, Antoni <bouanto@zoho.com>
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <http://www.gnu.org/licenses/>.
 -}

{-# LANGUAGE NamedFieldPuns #-}

{-|
Module: Escape
Description: Escape analyzer.
-}

module Escape (Escape, Functions, escapeAnalysis) where

import Control.Monad (when, zipWithM)
import Control.Monad.State (State, gets, modify, runState)
import qualified Data.Map as Map
import Data.Maybe (fromJust, fromMaybe, isJust)

import Ast ( BinaryOperation(..)
           , Constant(..)
           , Definition(..)
           , Escape(..)
           , ExprEscape(..)
           , Expression
           , Expression'(..)
           , FunctionCall(..)
           , FunctionDefinition(..)
           , IfExpression(..)
           , InlineAssembly(..)
           , LetStatement(..)
           , Module
           , Pattern(..)
           , PatternPos
           , Statement(..)
           , StatementPos
           , StructFieldValue(..)
           , TupleField(..)
           , Ty(..)
           , UnaryOperation(..)
           , WithType(..)
           )
import Functions (compilerEscapingFunctions, compilerFunctionNames)
import MaybeUtils (fromJustOrFail)
import Position (WithPos(..))

-- |The escape record.
data EscapeRecord =
    EscapeRecord
    { callEscapes :: Bool
    , functions :: Functions
    , functionEscapesStatus :: Map.Map String Bool
    }

-- |The escape state.
type EscapeState = State EscapeRecord

type Functions = Map.Map String FunctionDefinition

-- |Add the to the state whether a function escapes or not.
addEscapeStatus :: String -> Bool -> EscapeState ()
addEscapeStatus funcName escape = do
    escapesStatus <- gets functionEscapesStatus
    let newFunctionEscapes = Map.insert funcName escape escapesStatus
    modify $ \s -> s { functionEscapesStatus = newFunctionEscapes }

-- |Add a function to the state.
addFunction :: FunctionDefinition -> EscapeState ()
addFunction function@FunctionDefinition { functionName } = do
    funcs <- gets functions
    modify $ \s -> s { functions = Map.insert (item functionName) function funcs }

-- |Get the escapes from an expression.
analyzeExpr :: Expression -> EscapeState [Escape]
analyzeExpr WithType { itemNode = WithPos { item =
        BinOp _ }} =
    return []
analyzeExpr WithType { itemNode = WithPos { item =
        Call _ FunctionCall { callArguments = [], callName }}} =
    return [EscapeConst $ item callName]
analyzeExpr WithType { itemNode = WithPos { item =
        Call _ FunctionCall { callArguments, callName }}} =
    let funcName = item callName in
    if funcName `elem` compilerFunctionNames || funcName `elem` compilerEscapingFunctions then
        return []
    else do
        func <- getFunction funcName
        let zippedArgs = zip [0..] callArguments
            escapingParams = escapingParameters func
            escapingArguments = map snd $ filter ((`elem` escapingParams) . fst) zippedArgs
            filtered = map (\e -> case item $ itemNode e of
                              Const Constant { constantName } -> Just $ EscapeConst $ item constantName
                              _ -> Nothing
                            ) escapingArguments
            constEscapes = map fromJust $ filter isJust filtered
        return constEscapes
analyzeExpr WithType { itemNode = WithPos { item =
        CharLiteral _ }} = return []
analyzeExpr WithType { itemType, itemNode = WithPos { item =
        Const Constant { constantName }}} =
    return [EscapeConst $ item constantName | typeEscape itemType]
analyzeExpr WithType { itemNode = WithPos { item =
        DoubleLiteral _ }} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        EnumExpression _}} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        Field TupleField { tupleName, tupleIndex, structFieldType }}} =
    return [EscapeField (item tupleName) tupleIndex | typeEscape structFieldType]
analyzeExpr WithType { itemNode = WithPos { item =
        GlobalString _ }} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        If IfExpression { elseStatements, trueStatements }}} = do
    let lastTrueStatement = last trueStatements
    elseEscapes <- case elseStatements of
                       Just statements -> collectStatement $ last statements
                       Nothing -> return []
    trueEscapes <- collectStatement lastTrueStatement
    return $ elseEscapes ++ trueEscapes
analyzeExpr WithType { itemNode = WithPos { item =
        InlineAsm _ }} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        IntegerLiteral _ }} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        MethodCall _ }} = fail "A method call should have been transformed to a function call."
analyzeExpr WithType { itemNode = WithPos { item =
        StringLiteral _ }} = return []
analyzeExpr WithType { itemNode = WithPos { item =
        StructExpression _ fields }} = do
    result <- mapM (analyzeExpr . fieldValue) fields
    return $ concat result
analyzeExpr WithType { itemNode = WithPos { item =
        Tuple _ _ exprs }} = do
    result <- mapM analyzeExpr exprs
    return $ concat result
analyzeExpr WithType { itemNode = WithPos { item =
        UnOp _ }} = return []

-- |Check if a call expression can create an escape (to be called with the expression of the last statement).
callExprEscapes :: Expression -> EscapeState Bool
callExprEscapes expr =
    case item $ itemNode expr of
         Call _ FunctionCall { callName } -> do
            let funcName = item callName
            escape <- getEscapeStatus funcName
            let funcEscapes = fromMaybe False escape
            return funcEscapes
         _ -> return False

-- |Clear the callEscapes state.
clearEscaping :: EscapeState ()
clearEscaping = modify $ \s -> s { callEscapes = False }

-- |Collect the escaping names from a module definition.
collectDef :: Definition -> EscapeState ()
collectDef (Function function) = collectFunc function
collectDef (MethodDefinition _ _) = fail "A method definition should have been transformed to a function definition."
collectDef (TypeDef _ _) = return ()

-- |Collect the escaping parameters from a function definition.
collectEscapingFuncParams :: FunctionDefinition -> EscapeState ()
collectEscapingFuncParams func@FunctionDefinition { functionParameters, functionStatements } = do
    let lastStatement = last $ itemNode functionStatements
    let escapingParameters = getEscapingParameters lastStatement (concatMap (namesFromParam . item) functionParameters)
    addFunction func { escapingParameters }

-- |Collect the escaping parameters from a module definition.
collectEscapingParams :: Definition -> EscapeState ()
collectEscapingParams (Function function) = collectEscapingFuncParams function
collectEscapingParams (MethodDefinition _ _) = fail "A method definition should have been transformed to a function definition."
collectEscapingParams (TypeDef _ _) = return ()

-- |Collect the escaping names from a function definition.
collectFunc :: FunctionDefinition -> EscapeState ()
collectFunc FunctionDefinition { functionName, functionStatements } = do
    let funcName = item functionName
    func <- getFunction funcName
    let lastStatement = last $ itemNode functionStatements
    functionEscapes <- collectStatement lastStatement
    addFunction func { functionEscapes }

-- |Collect the escaping names from the statement.
-- |It should be called with the last statement of a block.
collectStatement :: StatementPos -> EscapeState [Escape]
collectStatement WithPos { item = Expr expr} = analyzeExpr expr
collectStatement WithPos { item = Let _ } = return []

-- |Do an escape analysis.
escapeAnalysis :: Module -> (Module, Functions)
escapeAnalysis modul =
    let state = runState (escapeAnalysis' modul) initState
        newMod = fst state
        newFuncs = functions $ snd state
    in (newMod, newFuncs)
    where initState = EscapeRecord { callEscapes = False, functions = toFuncMap currentFunctions, functionEscapesStatus = Map.empty }
          toFuncMap = Map.fromList . map (\func@FunctionDefinition { functionName } -> (item functionName, func))
          currentFunctions = map toFunc $ filter isFunc modul
          isFunc def = case def of
                            Function _ -> True
                            _ -> False
          toFunc def = case def of
                            Function func -> func
                            _ -> error "Should be a function."

-- |Do an escape analysis in the escape state.
escapeAnalysis' :: Module -> EscapeState Module
escapeAnalysis' modul = do
    mapM_ collectEscapingParams modul
    mapM_ collectDef modul
    definitions <- mapM escapeDef modul
    mapM setCallEscapingFunctions definitions

-- |Escape a module definition.
escapeDef :: Definition -> EscapeState Definition
escapeDef (Function function) = Function <$> escapeFunc function
escapeDef (MethodDefinition _ _) = fail "A method definition should have been transformed to a function definition."
escapeDef typeDef@(TypeDef _ _) = return typeDef

-- |Escape a function definition.
escapeFunc :: FunctionDefinition -> EscapeState FunctionDefinition
escapeFunc FunctionDefinition { functionName, functionStatements } = do
    let funcName = item functionName
    func <- getFunction funcName
    functionEscapes <- getEscapes funcName
    escapedStatements <- mapM (escapeStatement functionEscapes) $ itemNode functionStatements
    let lastStatement = last escapedStatements
    newLastStatement <- escapeLastStatement lastStatement
    functionHasEscapes <- funcHasEscapes funcName
    let newStatements = functionStatements { itemNode = init escapedStatements ++ [newLastStatement] }
        newFunc = func { functionEscapes, functionHasEscapes, functionStatements = newStatements }
    addFunction newFunc
    return newFunc

-- |Set the escape attribute to True of an expression if it is an heap allocated value.
escapeExpr :: Expression -> EscapeState Expression
escapeExpr expr@WithType { itemNode = withPos@WithPos { item = expression }} = do
    newItem <-
        case expression of
             BinOp _ -> return expression
             Call _ call@FunctionCall { callName } -> do
                hasEscapes <- funcHasEscapes $ item callName
                if hasEscapes then
                    return $ Call Escape call
                else
                    return $ Call NoEscape call
             CharLiteral _ -> return expression
             Const _ -> return expression
             DoubleLiteral _ -> return expression
             EnumExpression _ -> return expression
             Field _ -> return expression
             GlobalString _ -> return expression
             If ifExpression@IfExpression { condition } -> do
                 newCond <- escapeExpr condition
                 newIf <- mapIfElse ifExpression $ \statements -> do
                     newLastStatement <- escapeLastStatement $ last statements
                     return $ init statements ++ [newLastStatement]
                 return $ If newIf { condition = newCond }
             InlineAsm _ -> return expression
             IntegerLiteral _ -> return expression
             MethodCall _ -> return expression
             StringLiteral _ -> return expression
             StructExpression _ _ -> fail "A struct should have been converted to tuple."
             Tuple structType _ tuple -> do
                 newExpr <- mapM escapeExpr tuple
                 return $ Tuple structType Escape newExpr
             UnOp _ -> return expression
    return expr { itemNode = withPos { item = newItem }}

-- |Set the escape attribute on the last statement.
escapeLastStatement :: StatementPos -> EscapeState StatementPos
escapeLastStatement withPos@WithPos { item = (Expr expression) } = do
    newExpr <- escapeExpr expression
    return withPos { item = Expr newExpr }
escapeLastStatement letSatement@WithPos { item = Let _ } = return letSatement

-- |Set the escape attribute to True in the pattern.
escapePattern :: PatternPos -> Expression -> [Escape] -> EscapeState Expression
escapePattern _ expr [] = return expr
escapePattern pat value@WithType { itemNode = node} escapes =
    case item pat of
         Hole -> return value  -- NOTE: nothing to do when the value is not used.
         Ident name ->
             if hasConstEscape escapes $ item name then
                 escapeExpr value
             else
                case item node of
                     Tuple structType escape exprs -> do
                         newExprs <- zipWithM (\expr index ->
                                 if hasTupleEscape escapes (item name) index then
                                    escapeExpr expr
                                 else
                                    return expr)
                                 exprs [0..]
                         return value { itemNode = node { item = Tuple structType escape newExprs }}
                     _ -> return value
         TuplePattern patterns ->
             case item node of
                  Tuple structType escape exprs -> do
                      newExprs <- mapM (\(p, v) -> escapePattern p v escapes) $ zip patterns exprs
                      return value { itemNode = node { item = Tuple structType escape newExprs }}
                  _ -> return value

-- |Set the escape attribute to True in the statement.
escapeStatement :: [Escape] -> StatementPos -> EscapeState StatementPos
escapeStatement escapes expr@WithPos { item = Expr withType@WithType { itemNode = withPos@WithPos { item = itemExpr }}} =
    case itemExpr of
         If ifExpression -> do
            newIf <-
                mapIfElse ifExpression $ \statements ->
                    mapM (escapeStatement escapes) statements
            return $ expr { item = Expr withType { itemNode = withPos { item = If newIf }}}
         _ -> return expr
escapeStatement escapes letWithPos@WithPos { item = Let letStatement@LetStatement { letStatementPattern, letStatementValue }} = do
    newExpr <- escapePattern letStatementPattern letStatementValue escapes
    return letWithPos { item = Let letStatement { letStatementValue = newExpr }}

-- |Check if an expression contains a parameter.
exprContainsParam :: Expression -> String -> Bool
exprContainsParam expr param =
    case item $ itemNode expr of
         Const Constant { constantName } -> item constantName == param
         If IfExpression { trueStatements, elseStatements } ->
            let lastTrueStatement = last trueStatements
                trueEscaping = case item lastTrueStatement of
                                    Expr trueExpr -> exprContainsParam trueExpr param
                                    _ -> False
                elseEscaping =
                    case elseStatements of
                         Just statements ->
                             let lastStatement = last statements in
                             case item lastStatement of
                                  Expr falseExpr -> exprContainsParam falseExpr param
                                  _ -> False
                         Nothing -> False
            in
            trueEscaping || elseEscaping
         _ -> False

-- |Check if an expression can create an escape (to be called with the expression of the last statement).
exprEscapes :: Expression -> EscapeState Bool
exprEscapes expr =
    case item $ itemNode expr of
         If IfExpression { trueStatements, elseStatements } -> do
            let lastTrueStatement = last trueStatements
            lastFalseStatementEscapes <-
                case elseStatements of
                     Just statements ->
                         let lastFalseStatement = last statements in
                         statementEscapes lastFalseStatement
                     Nothing -> return False
            lastTrueStatementEscapes <- statementEscapes lastTrueStatement
            return $ lastTrueStatementEscapes || lastFalseStatementEscapes
         Tuple _ _ [] -> return False
         Tuple {} -> return True
         _ -> return False

-- |Check if a function has escapes.
funcHasEscapes :: String -> EscapeState Bool
funcHasEscapes funcName
    | funcName `elem` compilerFunctionNames = return False
    | funcName `elem` compilerEscapingFunctions = return True
    | otherwise = do
        escape <- getEscapeStatus funcName
        case escape of
             Just result ->
                 return result
             Nothing -> do
                 func <- getFunction funcName
                 let escapes = functionEscapes func
                     lastStatement = last $ itemNode $ functionStatements func
                 lastStatementEscapes <- statementEscapes lastStatement
                 let result = not (null escapes) || lastStatementEscapes
                 addEscapeStatus funcName result
                 return result

-- |Get the escapes of a function.
getEscapes :: String -> EscapeState [Escape]
getEscapes funcName =
    if funcName `elem` compilerFunctionNames || funcName `elem` compilerEscapingFunctions then
        return []
    else do
        func <- getFunction funcName
        return $ functionEscapes func

-- |Get whether a function escapes.
getEscapeStatus :: String -> EscapeState (Maybe Bool)
getEscapeStatus funcName = do
    escapes <- gets functionEscapesStatus
    return $ Map.lookup funcName escapes

-- |Get the parameters that are returned from a function.
getEscapingParameters :: StatementPos -> [String] -> [Int]
getEscapingParameters statement params =
    case item statement of
         Expr expr -> map fst $ filter (exprContainsParam expr . snd) $ zip [0..] params
         Let _ -> []

-- |Get a function from the state.
getFunction :: String -> EscapeState FunctionDefinition
getFunction funcName = do
    funcs <- gets functions
    return $ fromJustOrFail ("getFunction function " ++ funcName ++ " should exists") $ Map.lookup funcName funcs

-- |Check if `escapes` contains a const escape named `name`.
hasConstEscape :: [Escape] -> String -> Bool
hasConstEscape escapes name = any (isConstEscape name) escapes

-- |Check if `escapes` contains a field escape named `structName`.`fieldName`.
hasTupleEscape :: [Escape] -> String -> Integer -> Bool
hasTupleEscape escapes tupleName fieldIndex = any (isFieldEscape tupleName fieldIndex) escapes

-- |Check if an escape is a const escape named `name`.
isConstEscape :: String -> Escape -> Bool
isConstEscape name (EscapeConst escapeName) = escapeName == name
isConstEscape _ (EscapeField _ _) = False

-- |Check if an escape is a tuple field (`tupleName`.`fieldIndex`).
isFieldEscape :: String -> Integer -> Escape -> Bool
isFieldEscape _ _ (EscapeConst _) = False
isFieldEscape tupleName fieldIndex (EscapeField tuple field) = tupleName == tuple && fieldIndex == field

-- |Apply a function to both the true statements and the else statements (if any).
mapIfElse :: IfExpression -> ([StatementPos] -> EscapeState [StatementPos]) -> EscapeState IfExpression
mapIfElse ifExpr@IfExpression { trueStatements, elseStatements } mapper = do
    newTrueStatements <- mapper trueStatements
    newElseStatements <- case elseStatements of
                              Just statements -> do
                                  newStatements <- mapper statements
                                  return $ Just newStatements
                              Nothing -> return Nothing
    return ifExpr { trueStatements = newTrueStatements, elseStatements = newElseStatements }

-- |Get the names from a param pattern.
namesFromParam :: Pattern -> [String]
namesFromParam Hole = []
namesFromParam (Ident ident) = [item ident]
namesFromParam (TuplePattern patterns) = concatMap (namesFromParam . item) patterns

-- |Set the attribute callEscapingFunctions on the functions calling escaping functions.
setCallEscapingFunctions :: Definition -> EscapeState Definition
setCallEscapingFunctions (Function function) = Function <$> visitFunction function
setCallEscapingFunctions (MethodDefinition _ _) = fail "A method definition should have been transformed to a function definition."
setCallEscapingFunctions typeDef@(TypeDef _ _) = return typeDef

-- |Set the callEscapes attribute to True.
setEscaping :: EscapeState ()
setEscaping = modify $ \s -> s { callEscapes = True }

-- |Check if a statement can create an escape (to be called with the last statement).
statementEscapes :: StatementPos -> EscapeState Bool
statementEscapes WithPos { item = Expr expr } = do
    res1 <- exprEscapes expr
    res2 <-
        if res1 then
            return False
        else
            callExprEscapes expr
    return $ res1 || res2
statementEscapes WithPos { item = Let _ } = return False

-- |Check if a type escapes (a tuple type escapes).
typeEscape :: Ty -> Bool
typeEscape (Ty "Bool") = False
typeEscape (Ty "Char") = False
typeEscape (Ty "Double") = False
typeEscape (Ty "Int") = False
typeEscape _ = True

-- |Visit an expression to collect the attribute callEscapingFunctions.
visitExpr :: Expression -> EscapeState ()
visitExpr WithType { itemNode = WithPos { item = expression }} =
    case expression of
        BinOp (BinaryOperation _ expr1 expr2) -> do
            visitExpr expr1
            visitExpr expr2
        Call _ FunctionCall { callArguments, callName } -> do
            hasEscapes <- funcHasEscapes $ item callName
            when hasEscapes
                setEscaping
            mapM_ visitExpr callArguments
        CharLiteral _ -> return ()
        Const _ -> return ()
        DoubleLiteral _ -> return ()
        EnumExpression _ -> return ()
        Field _ -> return ()
        GlobalString _ -> return ()
        If IfExpression { condition, elseStatements, trueStatements } -> do
            visitExpr condition
            mapM_ visitStatement trueStatements
            case elseStatements of
                 Just statements -> mapM_ visitStatement statements
                 Nothing -> return ()
        InlineAsm InlineAssembly { inputArguments } ->
            mapM_ visitExpr inputArguments
        IntegerLiteral _ -> return ()
        MethodCall _ -> fail "A method call should have been converted to a function call"
        StringLiteral _ -> return ()
        StructExpression _ _ -> fail "A struct should have been converted to a tuple"
        Tuple _ _ exprs ->
            mapM_ visitExpr exprs
        UnOp (UnaryOperation _ expr) -> visitExpr expr

-- |Visit a function to set the attribute callEscapingFunctions.
visitFunction :: FunctionDefinition -> EscapeState FunctionDefinition
visitFunction func@FunctionDefinition { functionStatements } = do
    clearEscaping
    mapM_ visitStatement $ itemNode functionStatements
    callEscapingFunctions <- gets callEscapes
    return func { callEscapingFunctions }

-- |Visit a statement to collect the attribute callEscapingFunctions.
visitStatement :: StatementPos -> EscapeState ()
visitStatement WithPos { item = Expr expr } = visitExpr expr
visitStatement WithPos { item = Let LetStatement { letStatementValue }} = visitExpr letStatementValue
